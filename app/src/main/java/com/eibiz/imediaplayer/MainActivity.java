package com.eibiz.imediaplayer;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.eibiz.imediaplayer.manager.CaptureManager;
import com.eibiz.imediaplayer.manager.DownloadManager;
import com.eibiz.imediaplayer.manager.ScheduleManager;
import com.eibiz.imediaplayer.manager.ScreenManager;
import com.eibiz.imediaplayer.manager.StatusManager;
import com.eibiz.imediaplayer.manager.IoTManager;
import com.eibiz.imediaplayer.model.ConfigInfo;
import com.eibiz.imediaplayer.model.ProfileSchedule;
import com.eibiz.imediaplayer.service.PlayerService;
import com.eibiz.imediaplayer.utility.AppHelper;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;
import com.eibiz.imediaplayer.utility.NetworkHelper;

import java.io.File;

import static com.eibiz.imediaplayer.manager.StatusManager.mIStatusListener;


public class MainActivity extends AppCompatActivity {
    private Context mainContext = null;
    private static final String ThisClassName = MainActivity.class.getSimpleName();
    public static DownloadManager downloadManager = null;
    public static CaptureManager captureManager = null;
    public static StatusManager statusManager = null;
    public static IoTManager iotManager = null;
    public static ScheduleManager scheduleManager = null;

    StatusManager.IStatusListener mIStatusListener = new StatusManager.IStatusListener() {
        @Override
        public void DownloadComplete(StatusManager.StatusEvent e) {
            try {
                scheduleManager.updateSchedule();
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIStatusListener.DownloadComplete", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void DisplayQRCode(StatusManager.StatusEvent e) {
            try {
                mainHandler_sendMsgDelay(MainHandler.DrawQRCode, null, 0);
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIStatusListener.DisplayQRCode", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void CompleteRegistration(StatusManager.StatusEvent e) {
            try {
                mainHandler_sendMsgDelay(MainHandler.CompleteRegistration, null, 0);
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIStatusListener.CompleteRegistration", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void SettingComplete(StatusManager.StatusEvent e, String path) {
            try {
                LogHandler.writeLog(ThisClassName, "mIStatusListener.SettingComplete", LOG_TYPE.INFO, "Execute new App");

                Thread.sleep(2000);
                AppHelper.executeSetting(path);
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIStatusListener.SettingComplete", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };

    ScheduleManager.IScheduleListener mIScheduleListener = new ScheduleManager.IScheduleListener() {
        @Override
        public void ScheduleEnter(Object sender, ScheduleManager.ScheduleEvent e) {
            try {
                mainHandler_sendMsgDelay(MainHandler.DrawScreen, sender, 2000);

                // send api to display current playlist via drawScreen method execute
                SignageSystem.local.curSchedule = (ProfileSchedule) sender;
                PlayerService.updatePlayerStatus();
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIScheduleListener.ScheduleEnter", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void ScheduleExit(Object sender, ScheduleManager.ScheduleEvent e) {
            try {
                mainHandler_sendMsgDelay(MainHandler.DrawEmpty, sender, 1000);

                // send api to display blackscreen via drawEmpty method execute
                SignageSystem.local.curSchedule = new ProfileSchedule();
                PlayerService.updatePlayerStatus();
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIScheduleListener.ScheduleExit", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };

    private class MainHandler {
        private static final int SetStatusText = 0;
        private static final int DrawScreen = 1;
        private static final int DrawEmpty = 2;
        private static final int DrawQRCode = 3;
        private static final int CompleteRegistration = 4;
    }

    private Handler mainHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                if (msg.what == MainHandler.SetStatusText) {
                    ScreenManager.txtVwAndroidPlayerStatus.setText((String) msg.obj);
                } else if(msg.what == MainHandler.DrawEmpty || msg.what == MainHandler.CompleteRegistration) {
                    ProfileSchedule ps = (ProfileSchedule)msg.obj;
                    ScreenManager.drawEmpty(ps);
                } else if(msg.what == MainHandler.DrawScreen) {
                    ProfileSchedule sch = (ProfileSchedule)msg.obj;
                    if(sch != null) {
                        ScreenManager.drawScreen(sch);
                    }
                } else if(msg.what == MainHandler.DrawQRCode) {
                    ScreenManager.drawQRCode();
                }
            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "mainHandler.handleMessage", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };

    private void mainHandler_sendMsgDelay(int what, Object obj, long delayMillis) {
        Message msg = Message.obtain(mainHandler, what, obj);
        mainHandler.sendMessageDelayed(msg, delayMillis);
    }

    private class StatusThread extends Thread {
        @Override
        public void run() {
            mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Start i-Media Player", 0);

            ScreenManager.txtVwAndroidVersionName.setText("version: " + SignageSystem.version);
            ScreenManager.txtVwAndroidMacAddress.setText("macAddress: " + NetworkHelper.getMACAddress("wlan0"));

            // Create Download Manager:
            try {
                Thread.sleep(1000);
                mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Create Download Manager, please wait...", 0);
                downloadManager = new DownloadManager();
            } catch (Exception e) {
                LogHandler.writeLog(ThisClassName, "Run[Step01]", LOG_TYPE.INTERNAL_ERROR, e.getMessage());
            }
            if(ConfigInfo.connectionType.equals(ConfigInfo.ConnectionType.RestAPI)) {
                // Create Status Manager:
                try {
                    Thread.sleep(1000);
                    mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Create Status Manager, please wait...", 0);
                    statusManager = new StatusManager();
                    statusManager.setIStatusListener(mIStatusListener);
                } catch (Exception ex) {
                    LogHandler.writeLog(ThisClassName, "Run[Step02-01]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                }
                // Create Capture Manager:
                try {
                    Thread.sleep(1000);
                    captureManager = new CaptureManager();
                } catch (Exception ex) {
                    LogHandler.writeLog(ThisClassName, "Run[Step03]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                }
            } else if(ConfigInfo.connectionType.equals(ConfigInfo.ConnectionType.IoT)) {
                // Create IoT Manager:
                try {
                    Thread.sleep(1000);
                    mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Create IoT Manager, please wait...", 0);
                    iotManager = new IoTManager();

                } catch (Exception ex) {
                    LogHandler.writeLog(ThisClassName, "Run[Step02-02]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                }
            }

            /*
            // Create Network Recovery Manager:
            try {
                if (NetworkHelper.RootHelper.isRooted()) {
                    Thread.sleep(1000);
                    mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Create Network Recovery Manager, please wait...", 0);
                    //networkRecoveryManager = new NetworkRecoveryManager(mainContext);
                }
            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "Run[Step03]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }*/

            // Create Schedule Manager:
            try {
                Thread.sleep(1000);
                mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Create Schedule Manager, please wait...", 0);
                scheduleManager = new ScheduleManager();
                scheduleManager.setIScheduleListener(mIScheduleListener);
            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "Run[Step04]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }

            try {
                Thread.sleep(1000);
                mainHandler_sendMsgDelay(MainHandler.SetStatusText, "Initialize completed", 0);
            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "Run[Step05]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }

            try {
                Thread.sleep(500);
                mainHandler_sendMsgDelay(MainHandler.DrawEmpty, null, 0);
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "Run[Step06]", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainContext = this;
        SignageSystem.storagePath = this.getFilesDir().getAbsolutePath() + File.separator;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setUiLayoutFullscreen();
        LogHandler.writeLog(ThisClassName, "onCreate", LOG_TYPE.INFO, "start auroraplayer (v " + SignageSystem.version + ")");
        LogHandler.RemoveOldLogFile();
        LogHandler.writeLog(ThisClassName, "onCreate", LOG_TYPE.INFO, "after called RemoveOldLogFile()");
        ScreenManager screenManager = new ScreenManager(mainContext);

        try {
            // read information of model:
            if (SignageSystem.initData()) {
                screenManager.drawIntro();

                StatusThread statusThread = new StatusThread();
                statusThread.setDaemon(true);
                statusThread.start();
            }
        } catch (Exception e) {
            LogHandler.writeLog(ThisClassName, "onCreate", LOG_TYPE.INFO, "catch: SignageSystem.initData()");
        }

        setDeviceInfo();

        mSetContentView(screenManager.curDisplayScreen);
    }
    private void setDeviceInfo() {
        // set IPAddress
        SignageSystem.local.player.ipAddress = NetworkHelper.getLocalIpAddress();
        // set MacAddress
        SignageSystem.local.player.macAddress = NetworkHelper.getMACAddress("wlan0");
        // set Location
        Location location =  getLocation();
        if(location != null) {
            SignageSystem.latitude = location.getLatitude();
            SignageSystem.longitude = location.getLongitude();
        }
        // set appVersion
        SignageSystem.local.player.appVersion = SignageSystem.version;
    }
    public void mSetContentView(View view) { // mai: OK2
        setContentView(view);
    }
    private void setUiLayoutFullscreen() {
        if (Build.VERSION.SDK_INT >= 14) {
            // Get the DecorView of the associated Activity.
            View decorView = getWindow().getDecorView();
            // Immersively use these Flags.


            int uiOptions = // Hide the Status Bar.
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            // Hide the Navigation Bar.
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            // Make Content Appear Behind the Status Bar.
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            // Make Content Appear Behind the Navigation Bar.
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            // To help your app maintain a stable layout.
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE;

            if (Build.VERSION.SDK_INT >= 19) {
                uiOptions = uiOptions |
                        // To remain interactive when hiding the Status Bar and/or hiding the Navigation Bar.
                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public Location getLocation() {
        try {
            LocationManager lm = (LocationManager) getSystemService(mainContext.LOCATION_SERVICE);
            //Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            //lm.requestLocationUpdates(LocationManager.GPS, 100, 1, locationListener);
            Criteria criteria = new Criteria();
            String bestProvider = lm.getBestProvider(criteria, false);
            if(bestProvider == null)
                return null;
            Location loc = lm.getLastKnownLocation(bestProvider);
            return loc;
        } catch (SecurityException ex) { System.out.println(ex.getMessage());}
        return null;
    }
/*
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            SignageSystem.local.player.latitude = location.getLatitude();
            SignageSystem.local.player.longitude = location.getLongitude();
        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
