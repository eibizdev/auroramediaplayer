package com.eibiz.imediaplayer;
import android.os.Environment;

import com.eibiz.imediaplayer.model.DeviceSetting;
import com.eibiz.imediaplayer.model.IoTData;
import com.eibiz.imediaplayer.model.LocalData;
import com.eibiz.imediaplayer.model.Player;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.model.ProfileSchedule;
import com.eibiz.imediaplayer.model.ServerData;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class SignageSystem {
    private static final String ThisClassName = SignageSystem.class.getSimpleName();

    public static String version = "0.0.7"; // IoT Added
    public static String storagePath = "";
    public static String backup_storagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + File.separator;
    public static LocalData local = new LocalData();
    public static ServerData server = new ServerData();
    public static IoTData IoTCertification = new IoTData();

    public static String SettingPath = "setting" + File.separator;
    public static String MediaPath = "content" + File.separator;
    public static String AppPath = "app" + File.separator;
    public static String CapturePath = "capture" + File.separator;
    public static String keystorePath = "key" + File.separator;

    public static String DeviceSettingJsonPath = SettingPath + "deviceSetting.json";
    public static String PlayerJsonPath = SettingPath + "player.json";
    public static String ScheduleJsonPath = SettingPath + "schedule.json";
    public static String PlaylistMediaJsonPath = SettingPath + "playlist.json";
    public static String TransferLogJsonPath = SettingPath + "transferLogs.json";

    public static int fullScreenWidth = 0;
    public static int fullScreenHeight = 0;

    public static double latitude = 0;
    public static double longitude = 0;

    public static boolean download_cancel = false;

    public static int Screenshot_Interval = 30;

    public static boolean initData(){
        try {
            loadJson();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "initData", LOG_TYPE.IO_ERROR , ex.getMessage());
            return false;
        }
        return true;
    }
    public static void saveData() {
        try {
            saveJson();
            saveBackupFolder();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "saveData", LOG_TYPE.IO_ERROR , ex.getMessage());
        }
    }
    public static void saveDeviceSetting() {
        try {
            saveDeviceSettingJson();
            saveBackupDeviceSettingJson();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "saveDeviceSetting", LOG_TYPE.IO_ERROR , ex.getMessage());
        }
    }
    public static void saveTransferLog() {
        try {
            saveTransferLogJson();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "saveTransferLog", LOG_TYPE.IO_ERROR , ex.getMessage());
        }
    }


    public static void loadJson() throws Exception {
        FileInputStream fis = null;
        File file = null;
        FileChannel fc = null;
        MappedByteBuffer bb = null;
        // deviceSetting
        try {
            file = new File(storagePath + DeviceSettingJsonPath);
            if (file.exists()) {
                fis = new FileInputStream(file);
                fc = fis.getChannel();
                bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                String jsonStr = Charset.defaultCharset().decode(bb).toString();
                local.deviceSetting = new Gson().fromJson(jsonStr, DeviceSetting.class);
            }
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-loadJson[step01]> : " + ex.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step02]> : " + ex.getMessage());
                }
            }
            if(file != null)
                file = null;
            if(fc != null) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step03]> : " + ex.getMessage());
                }
            }
            if(bb != null) {
                bb.clear();
                bb = null;
            }
        }
        // player
        try {
            file = new File(storagePath + PlayerJsonPath);
            if (file.exists()) {
                fis = new FileInputStream(file);
                fc = fis.getChannel();
                bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                String jsonStr = Charset.defaultCharset().decode(bb).toString();
                local.player = new Gson().fromJson(jsonStr, Player.class);
                local.player.idCompany = 0;
            }
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-loadJson[step04]> : " + ex.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step05]> : " + ex.getMessage());
                }
            }
            if(file != null)
                file = null;
            if(fc != null) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step06]> : " + ex.getMessage());
                }
            }
            if(bb != null) {
                bb.clear();
                bb = null;
            }
        }
        // ProfileSchedule
        try {
            file = new File(storagePath + ScheduleJsonPath);
            if (file.exists()) {
                fis = new FileInputStream(file);
                fc = fis.getChannel();
                bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                String jsonStr = Charset.defaultCharset().decode(bb).toString();
                Type listType = new TypeToken<ArrayList<ProfileSchedule>>(){}.getType();
                local.schedules = new Gson().fromJson(jsonStr, listType);
            }
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-loadJson[step07]> : " + ex.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step08]> : " + ex.getMessage());
                }
            }
            if(file != null)
                file = null;
            if(fc != null) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step09]> : " + ex.getMessage());
                }
            }
            if(bb != null) {
                bb.clear();
                bb = null;
            }
        }
        // ProfilePlaylistMedia
        try {
            file = new File(storagePath + PlaylistMediaJsonPath);
            if (file.exists()) {
                fis = new FileInputStream(file);
                fc = fis.getChannel();
                bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                String jsonStr = Charset.defaultCharset().decode(bb).toString();
                Type listType = new TypeToken<ArrayList<ProfilePlaylistMedia>>(){}.getType();
                local.playlistMedias = new Gson().fromJson(jsonStr, listType);
            }
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-loadJson[step10]> : " + ex.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step11]> : " + ex.getMessage());
                }
            }
            if(file != null)
                file = null;
            if(fc != null) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step12]> : " + ex.getMessage());
                }
            }
            if(bb != null) {
                bb.clear();
                bb = null;
            }
        }
        // TransferLog
        try {
            file = new File(storagePath + TransferLogJsonPath);
            if (file.exists()) {
                fis = new FileInputStream(file);
                fc = fis.getChannel();
                bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                String jsonStr = Charset.defaultCharset().decode(bb).toString();
                Type listType = new TypeToken<ArrayList<ProfilePlaylistMedia>>(){}.getType();
                local.transferLogs = new Gson().fromJson(jsonStr, listType);
            }
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-loadJson[step13]> : " + ex.getMessage());
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step14]> : " + ex.getMessage());
                }
            }
            if(file != null)
                file = null;
            if(fc != null) {
                try {
                    fc.close();
                    fc = null;
                } catch (IOException ex) {
                    throw new Exception("<SignageSystem-loadJson[step15]> : " + ex.getMessage());
                }
            }
            if(bb != null) {
                bb.clear();
                bb = null;
            }
        }
    }
    public static void saveJson() throws Exception {
        File dir = new File(storagePath + SettingPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(storagePath + PlayerJsonPath);
            fw.write(new Gson().toJson(server.player));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveJson[step01]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            fw = new FileWriter(SignageSystem.storagePath + ScheduleJsonPath);
            fw.write(new Gson().toJson(server.schedules));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveJson[step02]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            fw = new FileWriter(SignageSystem.storagePath + PlaylistMediaJsonPath);
            fw.write(new Gson().toJson(server.playlistMedias));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveJson[step03]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void saveBackupFolder() throws Exception {
        File dir = new File(backup_storagePath + SettingPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(backup_storagePath + PlayerJsonPath);
            fw.write(new Gson().toJson(server.player));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveBackupFolder[step01]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            fw = new FileWriter(SignageSystem.backup_storagePath + ScheduleJsonPath);
            fw.write(new Gson().toJson(server.schedules));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveBackupFolder[step02]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            fw = new FileWriter(SignageSystem.backup_storagePath + PlaylistMediaJsonPath);
            fw.write(new Gson().toJson(server.playlistMedias));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveBackupFolder[step03]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void saveDeviceSettingJson() throws Exception {
        File dir = new File(storagePath + SettingPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(storagePath + DeviceSettingJsonPath);
            fw.write(new Gson().toJson(local.deviceSetting));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveDeviceSettingJson[step01]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void saveBackupDeviceSettingJson() throws Exception {
        File dir = new File(backup_storagePath + SettingPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(backup_storagePath + DeviceSettingJsonPath);
            fw.write(new Gson().toJson(local.deviceSetting));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveBackupDeviceSettingJson[step01]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } // finally
    }
    public static void saveTransferLogJson() throws Exception {
        File dir = new File(storagePath + SettingPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(storagePath + TransferLogJsonPath);
            fw.write(new Gson().toJson(local.transferLogs));
        } catch(Exception ex) {
            throw new Exception("<SignageSystem-saveTransferLogJson[step01]> : " + ex.getMessage());
        } finally {
            if (fw != null) {
                try {
                    fw.flush();
                    fw.close();
                    fw = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } // finally
    }
}
