package com.eibiz.imediaplayer.service;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

public class ContentService {
    public static boolean getContent(String fileName, String temp_url) {
        File directory = new File(SignageSystem.storagePath + SignageSystem.MediaPath);
        if (!directory.exists()) {
            directory.mkdir();
        }

        try {
            String temp_ = temp_url.replace(" ", "%20");
            BufferedInputStream inputStream = new BufferedInputStream(new URL( temp_).openStream());
            FileOutputStream fileOS = new FileOutputStream(SignageSystem.storagePath + SignageSystem.MediaPath + fileName);

            byte data[] = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileOS.write(data, 0, byteContent);
            }
        } catch (IOException e) {
            // handles IO exceptions
            LogHandler.writeLog("ContentService", "getContent", LOG_TYPE.NETWORK + "[step01]", e.toString());
            return false;
        } catch (Exception e) {
            LogHandler.writeLog("ContentService", "getContent", LOG_TYPE.NETWORK + "[step02]", e.toString());
            return false;
        }
        return true;
    }
}
