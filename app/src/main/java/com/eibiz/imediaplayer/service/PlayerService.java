package com.eibiz.imediaplayer.service;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.manager.ScreenManager;
import com.eibiz.imediaplayer.model.ConfigInfo;
import com.eibiz.imediaplayer.model.Player;
import com.eibiz.imediaplayer.utility.DateHelper;
import com.eibiz.imediaplayer.utility.DeviceHelper;
import com.eibiz.imediaplayer.utility.HttpService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;
import com.eibiz.imediaplayer.utility.NetworkHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class PlayerService {
    private static final String ThisClassName = PlayerService.class.getSimpleName();

    public static Player createPlayer() {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/createPlayer";

        HttpService httpService = new HttpService(url,"POST");
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            jSONObjSent.put("name","AndroidPlayer_" + NetworkHelper.getMACAddress("wlan0"));
            jSONObjSent.put("ipAddress", NetworkHelper.getLocalIpAddress());
            jSONObjSent.put("macAddress", NetworkHelper.getMACAddress("wlan0"));
            jSONObjSent.put("status", SignageSystem.local.player.status);
            jSONObjSent.put("reason", SignageSystem.local.player.reason);
            // calculate screenRitio
            int factor = ScreenManager.greatestCommonFactor( SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight);
            int widthRatio = SignageSystem.fullScreenWidth / factor;
            int heightRatio = SignageSystem.fullScreenHeight / factor;
            jSONObjSent.put("screenRatio", widthRatio + " : " + heightRatio);
            jSONObjSent.put("screenDimension", SignageSystem.fullScreenWidth  + " x " + SignageSystem.fullScreenHeight);
            jSONObjSent.put("deviceModel", DeviceHelper.getDeviceModel());
            jSONObjSent.put("appVersion", SignageSystem.version);
            jSONObjSent.put("createBy", "Device");

            jSONObjSent.put("latitude", SignageSystem.latitude);
            jSONObjSent.put("longitude", SignageSystem.longitude);

            if (!httpService.sendRequest(jSONObjSent.toString())) {
                LogHandler.writeLog(ThisClassName, "createPlayer", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");

                return new Player();
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
                JSONObject jSONObjResponse = new JSONObject(strJSONResponse);

                Player player = new Player();
                player.idPlayer = jSONObjResponse.getJSONObject("player").getInt("idPlayer");
                player.idCompany = jSONObjResponse.getJSONObject("player").getInt("idCompany");
                player.name = jSONObjResponse.getJSONObject("player").getString("name");
                //player.ipAddress = jSONObjResponse.getJSONObject("player").getString("ipAddress");
                //player.macAddress = jSONObjResponse.getJSONObject("player").getString("macAddress");
                player.status = jSONObjResponse.getJSONObject("player").getString("status");
                player.reason = jSONObjResponse.getJSONObject("player").getString("reason");
                //player.screenRatio = jSONObjResponse.getJSONObject("body").getString("screenRatio");
                //player.screenDimension = jSONObjResponse.getJSONObject("body").getString("screenDimension");
                //player.deviceModel = jSONObjResponse.getJSONObject("body").getString("deviceModel");
                //player.appVersion = jSONObjResponse.getJSONObject("player").getString("appVersion");

                return player;
            } else {
                LogHandler.writeLog(ThisClassName, "createPlayer", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
                return new Player();
            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "createPlayer", LOG_TYPE.NETWORK, ex.getMessage());

            return new Player();
        }
    }

    public static void updatePlayerStatus() {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/updateViewerInfo";

        HttpService httpService = new HttpService(url,"PUT");
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            jSONObjSent.put("idPlayer",SignageSystem.local.player.idPlayer);

            JSONObject playerJSONObjSent = new JSONObject();
            playerJSONObjSent.put("ipAddress", SignageSystem.local.player.ipAddress);
            playerJSONObjSent.put("status", SignageSystem.local.player.status);
            playerJSONObjSent.put("reason", SignageSystem.local.player.reason);
            playerJSONObjSent.put("appVersion", SignageSystem.local.player.appVersion);
            jSONObjSent.put("player", playerJSONObjSent);

            if(SignageSystem.local.curSchedule.idProfileSchedule == 0) {
                jSONObjSent.put("nowPlaying", null);
            } else {
                JSONObject nowPlayingJSONObjSent = new JSONObject();
                nowPlayingJSONObjSent.put("idSchedule", SignageSystem.local.curSchedule.idSchedule);
                nowPlayingJSONObjSent.put("idPlaylist", SignageSystem.local.curSchedule.idPlaylist);
                nowPlayingJSONObjSent.put("name", SignageSystem.local.curSchedule.playlistName);
                String startUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeStartDateTime);
                nowPlayingJSONObjSent.put("activeStartDateTime", startUTCDateTime);
                String endUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeEndDateTime);
                nowPlayingJSONObjSent.put("activeEndDateTime", endUTCDateTime);
                jSONObjSent.put("nowPlaying", nowPlayingJSONObjSent);
            }

            if(SignageSystem.local.curPublishProfile.idPlayerPublishHistory == 0) {
                jSONObjSent.put("idPlayerPublishHistory", null);
            } else {
                jSONObjSent.put("idPlayerPublishHistory", SignageSystem.local.curPublishProfile.idPlayerPublishHistory);
            }

            jSONObjSent.put("playerResource", null);
            jSONObjSent.put("updateBy", "Device");

            if (!httpService.sendRequest(jSONObjSent.toString())) {
                LogHandler.writeLog(ThisClassName, "updatePlayerStatus", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
                JSONObject jSONObjResponse = new JSONObject(strJSONResponse);
                if(!jSONObjResponse.isNull("idCompany")) {
                    SignageSystem.local.player.idCompany = jSONObjResponse.getInt("idCompany");
                } else {
                    SignageSystem.local.player.idCompany = 0;
                }
                if(!jSONObjResponse.isNull("lastPlayerPublish")) {                // publish request
                    SignageSystem.server.publishProfile.idPlayerPublishHistory = jSONObjResponse.getJSONObject("lastPlayerPublish").getInt("idPlayerPublishHistory");
                    SignageSystem.server.publishProfile.status = jSONObjResponse.getJSONObject("lastPlayerPublish").getString("status");
                }
                if(!jSONObjResponse.isNull("historyPlayerPublish")) {             // previous publish history info
                    SignageSystem.local.curPublishProfile.status = jSONObjResponse.getJSONObject("historyPlayerPublish").getString("status");
                }
            } else {
                LogHandler.writeLog(ThisClassName, "updatePlayerStatus", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
            }

        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "updatePlayerStatus", LOG_TYPE.NETWORK, ex.getMessage());
        }
    }

    //
    // IoT Services

    public static Player createIoTPlayer() {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/createPlayerNoVpc";

        HttpService httpService = new HttpService(url,"POST");
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            jSONObjSent.put("name","AndroidPlayer_" + NetworkHelper.getMACAddress("wlan0"));
            jSONObjSent.put("ipAddress", NetworkHelper.getLocalIpAddress());
            jSONObjSent.put("macAddress", NetworkHelper.getMACAddress("wlan0"));
            jSONObjSent.put("status", SignageSystem.local.player.status);
            jSONObjSent.put("reason", SignageSystem.local.player.reason);
            // calculate screenRitio
            int factor = ScreenManager.greatestCommonFactor( SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight);
            int widthRatio = SignageSystem.fullScreenWidth / factor;
            int heightRatio = SignageSystem.fullScreenHeight / factor;
            jSONObjSent.put("screenRatio", widthRatio + " : " + heightRatio);
            jSONObjSent.put("screenDimension", SignageSystem.fullScreenWidth  + " x " + SignageSystem.fullScreenHeight);
            jSONObjSent.put("deviceModel", DeviceHelper.getDeviceModel());
            jSONObjSent.put("appVersion", SignageSystem.version);
            jSONObjSent.put("createBy", "Device");

            jSONObjSent.put("latitude", SignageSystem.latitude);
            jSONObjSent.put("longitude", SignageSystem.longitude);

            if (!httpService.sendRequest(jSONObjSent.toString())) {
                LogHandler.writeLog(ThisClassName, "createPlayer", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");

                return new Player();
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
                JSONObject jSONObjResponse = new JSONObject(strJSONResponse);

                Player player = new Player();
                player.idPlayer = jSONObjResponse.getJSONObject("player").getInt("idPlayer");
                player.idCompany = jSONObjResponse.getJSONObject("player").getInt("idCompany");
                player.name = jSONObjResponse.getJSONObject("player").getString("name");
                //player.ipAddress = jSONObjResponse.getJSONObject("player").getString("ipAddress");
                //player.macAddress = jSONObjResponse.getJSONObject("player").getString("macAddress");
                player.status = jSONObjResponse.getJSONObject("player").getString("status");
                player.reason = jSONObjResponse.getJSONObject("player").getString("reason");
                //player.screenRatio = jSONObjResponse.getJSONObject("body").getString("screenRatio");
                //player.screenDimension = jSONObjResponse.getJSONObject("body").getString("screenDimension");
                //player.deviceModel = jSONObjResponse.getJSONObject("body").getString("deviceModel");
                //player.appVersion = jSONObjResponse.getJSONObject("player").getString("appVersion");

                SignageSystem.IoTCertification.thingName = jSONObjResponse.getJSONObject("thing").getString("thingName");
                SignageSystem.IoTCertification.thingArn = jSONObjResponse.getJSONObject("thing").getString("thingArn");
                SignageSystem.IoTCertification.thingId = jSONObjResponse.getJSONObject("thing").getString("thingId");
                SignageSystem.IoTCertification.certificateArn = jSONObjResponse.getJSONObject("thing").getString("certificateArn");
                SignageSystem.IoTCertification.certificateId = jSONObjResponse.getJSONObject("thing").getString("certificateId");
                SignageSystem.IoTCertification.certificatePem = jSONObjResponse.getJSONObject("thing").getString("certificatePem");
                SignageSystem.IoTCertification.publicKey = jSONObjResponse.getJSONObject("thing").getString("publicKey");
                SignageSystem.IoTCertification.privateKey = jSONObjResponse.getJSONObject("thing").getString("privateKey");

                return player;
            } else {
                LogHandler.writeLog(ThisClassName, "createIoTPlayer", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
                return new Player();
            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "createIoTPlayer", LOG_TYPE.NETWORK, ex.getMessage());

            return new Player();
        }
    }

    public static boolean updateViewerInfoIoT() {
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            jSONObjSent.put("idPlayer",SignageSystem.local.player.idPlayer);

            JSONObject playerJSONObjSent = new JSONObject();
            playerJSONObjSent.put("ipAddress", SignageSystem.local.player.ipAddress);
            playerJSONObjSent.put("status", SignageSystem.local.player.status);
            playerJSONObjSent.put("reason", SignageSystem.local.player.reason);
            playerJSONObjSent.put("appVersion", SignageSystem.local.player.appVersion);
            jSONObjSent.put("player", playerJSONObjSent);

            if(SignageSystem.local.curSchedule.idProfileSchedule == 0) {
                jSONObjSent.put("nowPlaying", null);
            } else {
                JSONObject nowPlayingJSONObjSent = new JSONObject();
                nowPlayingJSONObjSent.put("idSchedule", SignageSystem.local.curSchedule.idSchedule);
                nowPlayingJSONObjSent.put("idPlaylist", SignageSystem.local.curSchedule.idPlaylist);
                nowPlayingJSONObjSent.put("name", SignageSystem.local.curSchedule.playlistName);
                String startUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeStartDateTime);
                nowPlayingJSONObjSent.put("activeStartDateTime", startUTCDateTime);
                String endUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeEndDateTime);
                nowPlayingJSONObjSent.put("activeEndDateTime", endUTCDateTime);
                jSONObjSent.put("nowPlaying", nowPlayingJSONObjSent);
            }

            if(SignageSystem.local.curPublishProfile.idPlayerPublishHistory == 0) {
                jSONObjSent.put("idPlayerPublishHistory", null);
            } else {
                jSONObjSent.put("idPlayerPublishHistory", SignageSystem.local.curPublishProfile.idPlayerPublishHistory);
            }

            jSONObjSent.put("playerResource", null);
            jSONObjSent.put("updateBy", "Device");

//            if (!httpService.sendRequest(jSONObjSent.toString())) {
//                LogHandler.writeLog(ThisClassName, "updatePlayerStatus", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
//            }
//
//            String strJSONResponse = httpService.getResponse();
//            if (!strJSONResponse.equals("error")) {
//                JSONObject jSONObjResponse = new JSONObject(strJSONResponse);
//                if(!jSONObjResponse.isNull("idCompany")) {
//                    SignageSystem.local.player.idCompany = jSONObjResponse.getInt("idCompany");
//                } else {
//                    SignageSystem.local.player.idCompany = 0;
//                }
//                if(!jSONObjResponse.isNull("lastPlayerPublish")) {                // publish request
//                    SignageSystem.server.publishProfile.idPlayerPublishHistory = jSONObjResponse.getJSONObject("lastPlayerPublish").getInt("idPlayerPublishHistory");
//                    SignageSystem.server.publishProfile.status = jSONObjResponse.getJSONObject("lastPlayerPublish").getString("status");
//                }
//                if(!jSONObjResponse.isNull("historyPlayerPublish")) {             // previous publish history info
//                    SignageSystem.local.curPublishProfile.status = jSONObjResponse.getJSONObject("historyPlayerPublish").getString("status");
//                }
//            } else {
//                LogHandler.writeLog(ThisClassName, "updatePlayerStatus", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
//            }

        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "updateViewerInfoIoT", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }
}
