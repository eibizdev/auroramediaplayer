package com.eibiz.imediaplayer.service;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ConfigInfo;
import com.eibiz.imediaplayer.utility.DateHelper;
import com.eibiz.imediaplayer.utility.HttpService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import org.json.JSONException;
import org.json.JSONObject;

public class CaptureService {
    private static final String ThisClassName = PlayerService.class.getSimpleName();
    public static boolean uploadScreenCapture(String base64) {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/uploadMedia";

        HttpService httpService = new HttpService(url,"POST");
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany", SignageSystem.local.player.idCompany);
            jSONObjSent.put("idPlayer", SignageSystem.local.player.idPlayer);
            jSONObjSent.put("purpose", "Screenshot");
            jSONObjSent.put("base64", "data:image/png;base64," + base64);
            if(SignageSystem.local.curSchedule.idProfileSchedule == 0) {
                jSONObjSent.put("nowPlaying", null);
            } else {
                JSONObject nowPlayingJSONObjSent = new JSONObject();
                nowPlayingJSONObjSent.put("idSchedule", SignageSystem.local.curSchedule.idSchedule);
                nowPlayingJSONObjSent.put("idPlaylist", SignageSystem.local.curSchedule.idPlaylist);
                nowPlayingJSONObjSent.put("name", SignageSystem.local.curSchedule.playlistName);
                String startUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeStartDateTime);
                nowPlayingJSONObjSent.put("activeStartDateTime", startUTCDateTime);
                String endUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeEndDateTime);
                nowPlayingJSONObjSent.put("activeEndDateTime", endUTCDateTime);
                jSONObjSent.put("nowPlaying", nowPlayingJSONObjSent);
            }

            if (!httpService.sendRequest(jSONObjSent.toString())) {
                LogHandler.writeLog(ThisClassName, "CaptureService", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
               return true;
            } else {
                LogHandler.writeLog(ThisClassName, "CaptureService", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "CaptureService", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }

    //
    // IoT Services
    public static boolean uploadMediaFileIoT(String base64) {
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany", SignageSystem.local.player.idCompany);
            jSONObjSent.put("idPlayer", SignageSystem.local.player.idPlayer);
            jSONObjSent.put("purpose", "Screenshot");
            jSONObjSent.put("base64", "data:image/png;base64," + base64);
            if(SignageSystem.local.curSchedule.idProfileSchedule == 0) {
                jSONObjSent.put("nowPlaying", null);
            } else {
                JSONObject nowPlayingJSONObjSent = new JSONObject();
                nowPlayingJSONObjSent.put("idSchedule", SignageSystem.local.curSchedule.idSchedule);
                nowPlayingJSONObjSent.put("idPlaylist", SignageSystem.local.curSchedule.idPlaylist);
                nowPlayingJSONObjSent.put("name", SignageSystem.local.curSchedule.playlistName);
                String startUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeStartDateTime);
                nowPlayingJSONObjSent.put("activeStartDateTime", startUTCDateTime);
                String endUTCDateTime = DateHelper.convertToUTC(SignageSystem.local.curSchedule.activeEndDateTime);
                nowPlayingJSONObjSent.put("activeEndDateTime", endUTCDateTime);
                jSONObjSent.put("nowPlaying", nowPlayingJSONObjSent);
            }

//            if (!httpService.sendRequest(jSONObjSent.toString())) {
//                LogHandler.writeLog(ThisClassName, "CaptureService", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
//            }
//
//            String strJSONResponse = httpService.getResponse();
//            if (!strJSONResponse.equals("error")) {
//                return true;
//            } else {
//                LogHandler.writeLog(ThisClassName, "CaptureService", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
//            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "uploadMediaFileIoT", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }
}
