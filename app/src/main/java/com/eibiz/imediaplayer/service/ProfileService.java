package com.eibiz.imediaplayer.service;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ConfigInfo;
import com.eibiz.imediaplayer.model.PlaylistMediaComparer;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.model.ProfileSchedule;
import com.eibiz.imediaplayer.utility.DateHelper;
import com.eibiz.imediaplayer.utility.HttpService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProfileService {
    private static final String ThisClassName = PlayerService.class.getSimpleName();

    public static void getPublishProfile(int idCompany, int idPlayer, int idPlayerPublishHistory) {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/getPublishProfile/" + idCompany + "/" + idPlayer + "/" + idPlayerPublishHistory;

        HttpService httpService = new HttpService(url,"GET");

        try {
            if (!httpService.sendRequest("")) {
                LogHandler.writeLog(ThisClassName, "getPublishProfile", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
                JSONObject jSONObjResponse = new JSONObject(strJSONResponse);

                SignageSystem.server.publishProfile.idPlayerPublishHistory = jSONObjResponse.getJSONObject("publishProfile").getInt("idPlayerPublishHistory");
                SignageSystem.server.publishProfile.status = jSONObjResponse.getJSONObject("publishProfile").getString("status");
                SignageSystem.server.publishProfile.reason = jSONObjResponse.getJSONObject("publishProfile").getString("reason");

                if(!jSONObjResponse.getJSONObject("publishProfile").isNull("profileSchedules")) {
                    JSONArray schedulesJson = jSONObjResponse.getJSONObject("publishProfile").getJSONArray("profileSchedules");

                    for (int i = 0; i < schedulesJson.length(); i++) {
                        ProfileSchedule ps = new ProfileSchedule();
                        ps.idProfileSchedule = schedulesJson.getJSONObject(i).getInt("idProfileSchedule");
                        ps.idPlayerPublishHistory = SignageSystem.server.publishProfile.idPlayerPublishHistory;
                        // playlist
                        if (!schedulesJson.getJSONObject(i).isNull("playlist")) {
                            ps.idPlaylist = schedulesJson.getJSONObject(i).getJSONObject("playlist").getInt("idPlaylist");
                            ps.playlistName = schedulesJson.getJSONObject(i).getJSONObject("playlist").getString("name");
                        }
                        // schedule
                        if (!schedulesJson.getJSONObject(i).isNull("schedule")) {
                            ps.idSchedule = schedulesJson.getJSONObject(i).getJSONObject("schedule").getInt("idSchedule");

                            ps.activeStartDate = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeStartDate");
                            ps.activeStartDate = DateHelper.convertToCurrentTimeZone(ps.activeStartDate);
                            ps.activeStartDate = ps.activeStartDate.split("\\s+")[0];
                            ps.activeEndDate = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeEndDate");
                            ps.activeEndDate = DateHelper.convertToCurrentTimeZone(ps.activeEndDate);
                            ps.activeEndDate = ps.activeEndDate.split("\\s+")[0];

                            ps.activeStartTime = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeStartTime");
                            ps.activeStartTime = DateHelper.convertToCurrentTimeZone(ps.activeStartDate + " " + ps.activeStartTime);
                            ps.activeStartDateTime = ps.activeStartDate + " " + ps.activeStartTime.split("\\s+")[1];

                            ps.activeEndTime = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeEndTime");
                            ps.activeEndTime = DateHelper.convertToCurrentTimeZone(ps.activeEndDate + " " + ps.activeEndTime);
                            ps.activeEndDateTime = ps.activeEndDate + " " + ps.activeEndTime.split("\\s+")[1];

                            ps.freqType = schedulesJson.getJSONObject(i).getJSONObject("schedule").getInt("freqType");
                            ps.ScheduleType = DateHelper.convertFreqSchedule(ps.freqType);
                            ps.freqInterval = schedulesJson.getJSONObject(i).getJSONObject("schedule").getInt("freqInterval");
                            if(ps.ScheduleType.equals(ProfileSchedule.Sch_Type.Weekly)) {
                                List<String> temp = DateHelper.convertFreqIntervalForWeekly(ps.freqInterval);
                                ps.ScheduleWeekly.addAll(temp);
                            } else if(ps.ScheduleType.equals(ProfileSchedule.Sch_Type.Monthly)) {
                                List<Integer> temp = DateHelper.convertFreqIntervalForMonthly(ps.freqInterval);
                                ps.ScheduleMonthly.addAll(temp);
                            }
                        }
                        // profilePlaylistMedias
                        if (!schedulesJson.getJSONObject(i).isNull("profilePlaylistMedias")) {
                            JSONArray profilePlaylistMediasJson = schedulesJson.getJSONObject(i).getJSONArray("profilePlaylistMedias");

                            for (int j = 0; j < profilePlaylistMediasJson.length(); j++) {
                                ProfilePlaylistMedia ppm = new ProfilePlaylistMedia();
                                ppm.idProfilePlaylistMedia = profilePlaylistMediasJson.getJSONObject(j).getInt("idProfilePlaylistMedia");
                                ppm.idProfileSchedule = ps.idProfileSchedule;
                                ppm.status = profilePlaylistMediasJson.getJSONObject(j).getString("status");
                                // playlistmedia
                                if (!profilePlaylistMediasJson.getJSONObject(j).isNull("playlistMedia")) {
                                    ppm.playlistMediaOrder = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("playlistMedia").getInt("order");
                                    ppm.playlistMediaLength = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("playlistMedia").getInt("length");
                                }
                                // media
                                if (!profilePlaylistMediasJson.getJSONObject(j).isNull("media")) {
                                    ppm.mediaName = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("name");
                                    ppm.mediaDisplayName = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("displayName");
                                    ppm.mediaType = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("type");
                                    ppm.mediaURL = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("URL");
                                    ppm.mediaSize = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getInt("size");
                                }
                                SignageSystem.server.playlistMedias.add(ppm);
                            }
                        }

                        SignageSystem.server.schedules.add(ps);
                    }
                }
            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "getPublishProfile", LOG_TYPE.NETWORK, ex.getMessage());
        }
    }

    public static boolean updateWorkStatus() {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/updateWorkStatus";

        HttpService httpService = new HttpService(url,"PUT");
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            JSONObject publishProfileJSONObjSent = new JSONObject();
            publishProfileJSONObjSent.put("idPlayerPublishHistory", SignageSystem.local.curPublishProfile.idPlayerPublishHistory);
            publishProfileJSONObjSent.put("status", SignageSystem.local.curPublishProfile.status);
            publishProfileJSONObjSent.put("reason", SignageSystem.local.curPublishProfile.reason);
            jSONObjSent.put("publishProfile", publishProfileJSONObjSent);
            jSONObjSent.put("updateBy", "Device");

            if (!httpService.sendRequest(jSONObjSent.toString())) {
                LogHandler.writeLog(ThisClassName, "updateWorkStatus", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
                System.out.println("[updateWorkStatus]: success to connect with Server.");
                return true;
            } else {
                LogHandler.writeLog(ThisClassName, "updateWorkStatus", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "updateWorkStatus", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }

    public static boolean updateProfilePlaylistMedia(ProfilePlaylistMedia ppm) {
        String url = ConfigInfo.serverIPAddress + "ViewerManager/updateProfilePlaylistMedia";

        HttpService httpService = new HttpService(url,"PUT");
        JSONObject jSONObjSent = new JSONObject();
        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            JSONObject profilePlaylistMediaJSONObjSent = new JSONObject();
            profilePlaylistMediaJSONObjSent.put("idProfilePlaylistMedia", ppm.idProfilePlaylistMedia);
            profilePlaylistMediaJSONObjSent.put("status", ppm.status);
            profilePlaylistMediaJSONObjSent.put("reason", ppm.reason);
            jSONObjSent.put("profilePlaylistMedia", profilePlaylistMediaJSONObjSent);
            jSONObjSent.put("updateBy", "Device");

            if (!httpService.sendRequest(jSONObjSent.toString())) {
                LogHandler.writeLog(ThisClassName, "updateProfilePlaylistMedia", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
            }

            String strJSONResponse = httpService.getResponse();
            if (!strJSONResponse.equals("error")) {
                System.out.println("[updateProfilePlaylistMedia]: success to connect with Server.");
                return true;
            } else {
                LogHandler.writeLog(ThisClassName, "updateProfilePlaylistMedia", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
            }
        } catch(JSONException ex) {
            LogHandler.writeLog(ThisClassName, "updateProfilePlaylistMedia", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }

    // local function
    public static List<ProfilePlaylistMedia> getProfilePlaylistMedias(int idProfileSchdule) {
        List<ProfilePlaylistMedia> ppms = new ArrayList<>();
        try{
            if(SignageSystem.local.playlistMedias != null) {
                for(int i = 0; i < SignageSystem.local.playlistMedias.size(); i++) {
                    ProfilePlaylistMedia tmpPPM = SignageSystem.local.playlistMedias.get(i);
                    if(tmpPPM.idProfileSchedule == idProfileSchdule)
                        ppms.add(tmpPPM);
                }

                Collections.sort(ppms, new PlaylistMediaComparer());
            }
        } catch(Exception e) {
            LogHandler.writeLog(ThisClassName, "getProfilePlaylistMedias", LOG_TYPE.INTERNAL_ERROR, e.toString());
        }
        return ppms;
    }

    //
    // IoT Services
    public static boolean updateProfilePlaylistMediaIoT(ProfilePlaylistMedia ppm) {
        JSONObject jSONObjSent = new JSONObject();
        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            JSONObject profilePlaylistMediaJSONObjSent = new JSONObject();
            profilePlaylistMediaJSONObjSent.put("idProfilePlaylistMedia", ppm.idProfilePlaylistMedia);
            profilePlaylistMediaJSONObjSent.put("status", ppm.status);
            profilePlaylistMediaJSONObjSent.put("reason", ppm.reason);
            jSONObjSent.put("profilePlaylistMedia", profilePlaylistMediaJSONObjSent);
            jSONObjSent.put("updateBy", "Device");

//            if (!httpService.sendRequest(jSONObjSent.toString())) {
//                LogHandler.writeLog(ThisClassName, "updateProfilePlaylistMedia", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
//            }
//
//            String strJSONResponse = httpService.getResponse();
//            if (!strJSONResponse.equals("error")) {
//                System.out.println("[updateProfilePlaylistMedia]: success to connect with Server.");
//                return true;
//            } else {
//                LogHandler.writeLog(ThisClassName, "updateProfilePlaylistMedia", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
//            }
        } catch(JSONException ex) {
            LogHandler.writeLog(ThisClassName, "updateProfilePlaylistMediaIoT", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }

    public static boolean updateWorkStatusIoT() {
        JSONObject jSONObjSent = new JSONObject();

        try {
            jSONObjSent.put("idCompany",SignageSystem.local.player.idCompany);
            JSONObject publishProfileJSONObjSent = new JSONObject();
            publishProfileJSONObjSent.put("idPlayerPublishHistory", SignageSystem.local.curPublishProfile.idPlayerPublishHistory);
            publishProfileJSONObjSent.put("status", SignageSystem.local.curPublishProfile.status);
            publishProfileJSONObjSent.put("reason", SignageSystem.local.curPublishProfile.reason);
            jSONObjSent.put("publishProfile", publishProfileJSONObjSent);
            jSONObjSent.put("updateBy", "Device");

//            if (!httpService.sendRequest(jSONObjSent.toString())) {
//                LogHandler.writeLog(ThisClassName, "updateWorkStatus", LOG_TYPE.NETWORK, "httpService.sendRequest()--Failed!");
//            }
//
//            String strJSONResponse = httpService.getResponse();
//            if (!strJSONResponse.equals("error")) {
//                System.out.println("[updateWorkStatus]: success to connect with Server.");
//                return true;
//            } else {
//                LogHandler.writeLog(ThisClassName, "updateWorkStatus", LOG_TYPE.NETWORK, "httpService.getResponse()--Failed!");
//            }
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "updateWorkStatusIoT", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }

    public static boolean newPublishIoT(String jsonStr) {
        try {
            JSONObject jSONObjResponse = new JSONObject(jsonStr);

            SignageSystem.server.publishProfile.idPlayerPublishHistory = jSONObjResponse.getJSONObject("publishProfile").getInt("idPlayerPublishHistory");
            SignageSystem.server.publishProfile.status = jSONObjResponse.getJSONObject("publishProfile").getString("status");
            SignageSystem.server.publishProfile.reason = jSONObjResponse.getJSONObject("publishProfile").getString("reason");

            if(!jSONObjResponse.getJSONObject("publishProfile").isNull("profileSchedules")) {
                JSONArray schedulesJson = jSONObjResponse.getJSONObject("publishProfile").getJSONArray("profileSchedules");

                for (int i = 0; i < schedulesJson.length(); i++) {
                    ProfileSchedule ps = new ProfileSchedule();
                    ps.idProfileSchedule = schedulesJson.getJSONObject(i).getInt("idProfileSchedule");
                    ps.idPlayerPublishHistory = SignageSystem.server.publishProfile.idPlayerPublishHistory;
                    // playlist
                    if (!schedulesJson.getJSONObject(i).isNull("playlist")) {
                        ps.idPlaylist = schedulesJson.getJSONObject(i).getJSONObject("playlist").getInt("idPlaylist");
                        ps.playlistName = schedulesJson.getJSONObject(i).getJSONObject("playlist").getString("name");
                    }
                    // schedule
                    if (!schedulesJson.getJSONObject(i).isNull("schedule")) {
                        ps.idSchedule = schedulesJson.getJSONObject(i).getJSONObject("schedule").getInt("idSchedule");

                        ps.activeStartDate = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeStartDate");
                        ps.activeStartDate = DateHelper.convertToCurrentTimeZone(ps.activeStartDate);
                        ps.activeStartDate = ps.activeStartDate.split("\\s+")[0];
                        ps.activeEndDate = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeEndDate");
                        ps.activeEndDate = DateHelper.convertToCurrentTimeZone(ps.activeEndDate);
                        ps.activeEndDate = ps.activeEndDate.split("\\s+")[0];

                        ps.activeStartTime = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeStartTime");
                        ps.activeStartTime = DateHelper.convertToCurrentTimeZone(ps.activeStartDate + " " + ps.activeStartTime);
                        ps.activeStartDateTime = ps.activeStartDate + " " + ps.activeStartTime.split("\\s+")[1];

                        ps.activeEndTime = schedulesJson.getJSONObject(i).getJSONObject("schedule").getString("activeEndTime");
                        ps.activeEndTime = DateHelper.convertToCurrentTimeZone(ps.activeEndDate + " " + ps.activeEndTime);
                        ps.activeEndDateTime = ps.activeEndDate + " " + ps.activeEndTime.split("\\s+")[1];

                        ps.freqType = schedulesJson.getJSONObject(i).getJSONObject("schedule").getInt("freqType");
                        ps.ScheduleType = DateHelper.convertFreqSchedule(ps.freqType);
                        ps.freqInterval = schedulesJson.getJSONObject(i).getJSONObject("schedule").getInt("freqInterval");
                        if(ps.ScheduleType.equals(ProfileSchedule.Sch_Type.Weekly)) {
                            List<String> temp = DateHelper.convertFreqIntervalForWeekly(ps.freqInterval);
                            ps.ScheduleWeekly.addAll(temp);
                        } else if(ps.ScheduleType.equals(ProfileSchedule.Sch_Type.Monthly)) {
                            List<Integer> temp = DateHelper.convertFreqIntervalForMonthly(ps.freqInterval);
                            ps.ScheduleMonthly.addAll(temp);
                        }
                    }
                    // profilePlaylistMedias
                    if (!schedulesJson.getJSONObject(i).isNull("profilePlaylistMedias")) {
                        JSONArray profilePlaylistMediasJson = schedulesJson.getJSONObject(i).getJSONArray("profilePlaylistMedias");

                        for (int j = 0; j < profilePlaylistMediasJson.length(); j++) {
                            ProfilePlaylistMedia ppm = new ProfilePlaylistMedia();
                            ppm.idProfilePlaylistMedia = profilePlaylistMediasJson.getJSONObject(j).getInt("idProfilePlaylistMedia");
                            ppm.idProfileSchedule = ps.idProfileSchedule;
                            ppm.status = profilePlaylistMediasJson.getJSONObject(j).getString("status");
                            // playlistmedia
                            if (!profilePlaylistMediasJson.getJSONObject(j).isNull("playlistMedia")) {
                                ppm.playlistMediaOrder = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("playlistMedia").getInt("order");
                                ppm.playlistMediaLength = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("playlistMedia").getInt("length");
                            }
                            // media
                            if (!profilePlaylistMediasJson.getJSONObject(j).isNull("media")) {
                                ppm.mediaName = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("name");
                                ppm.mediaDisplayName = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("displayName");
                                ppm.mediaType = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("type");
                                ppm.mediaURL = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getString("URL");
                                ppm.mediaSize = profilePlaylistMediasJson.getJSONObject(j).getJSONObject("media").getInt("size");
                            }
                            SignageSystem.server.playlistMedias.add(ppm);
                        }
                    }

                    SignageSystem.server.schedules.add(ps);
                }
            }

            return true;
        } catch (JSONException ex) {
            LogHandler.writeLog(ThisClassName, "newPublishIoT", LOG_TYPE.NETWORK, ex.getMessage());
        }
        return false;
    }
}
