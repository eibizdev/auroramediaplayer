package com.eibiz.imediaplayer.utility;
import com.amazonaws.mobileconnectors.iot.AWSIotKeystoreHelper;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ConfigInfo;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.util.EventObject;

import static com.eibiz.imediaplayer.SignageSystem.IoTCertification;

public class IoTService {
    private static final String ThisClassName = IoTService.class.getSimpleName();
    private static AWSIotMqttManager mqttManager;

    // Status EVENT.
    public class IoTServiceEvent extends EventObject {
        public IoTServiceEvent(Object source) {
            super(source);
        }
    }
    public interface IIoTServiceListener {
        void IoTConnectionFail(IoTServiceEvent e);
        void SubscribeMessageReceive(IoTServiceEvent e);
    }
    public static IIoTServiceListener mIIoTServiceListener = null;
    public void setIIoTServiceListener(IIoTServiceListener pIIoTServiceListener) {
        mIIoTServiceListener = pIIoTServiceListener;
    }

    public IoTService() {
        File dir = new File(SignageSystem.storagePath + SignageSystem.keystorePath);
        if (!dir.exists()) {
            dir.mkdir();
        }

        if((!AWSIotKeystoreHelper.isKeystorePresent(SignageSystem.storagePath + SignageSystem.keystorePath, SignageSystem.IoTCertification.keystoreName)) ||
                !AWSIotKeystoreHelper.keystoreContainsAlias(SignageSystem.IoTCertification.certificateId,SignageSystem.storagePath + SignageSystem.keystorePath,SignageSystem.IoTCertification.keystoreName,SignageSystem.IoTCertification.keystorePassword)) {
            createIotKeyStore();
        }
    }

    private void createIotKeyStore() {
        AWSIotKeystoreHelper.saveCertificateAndPrivateKey(
                SignageSystem.IoTCertification.certificateId,
                SignageSystem.IoTCertification.certificatePem,
                SignageSystem.IoTCertification.privateKey,
                SignageSystem.storagePath + SignageSystem.keystorePath,
                SignageSystem.IoTCertification.keystoreName,
                SignageSystem.IoTCertification.keystorePassword);
    }

    private KeyStore getKeyStore () throws Exception {
        KeyStore key = null;
        try {
            key = AWSIotKeystoreHelper.getIotKeystore(SignageSystem.IoTCertification.certificateId,SignageSystem.storagePath + SignageSystem.keystorePath, SignageSystem.IoTCertification.keystoreName,SignageSystem.IoTCertification.keystorePassword);
        } catch(Exception ex) {
            throw new Exception();
        }
        return key;
    }

    public void executeIoTStatusTask() {
        try {
            // generate IoTService
            KeyStore keyStore = getKeyStore();
            // MQTT Client
            mqttManager = new AWSIotMqttManager(IoTCertification.thingName, ConfigInfo.IoTServer);
            mqttManager.setAutoReconnect(true); //default is true
            mqttManager.setMaxAutoReconnectAttempts(-1); //default is -1 forever

            mqttManager.connect(keyStore, new AWSIotMqttClientStatusCallback() {
                @Override
                public void onStatusChanged(final AWSIotMqttClientStatus status, final Throwable throwable) {
                    System.out.println("Status = " + status);

                    if (status == AWSIotMqttClientStatus.Connecting) {
                        System.out.println("Connecting...");

                    } else if (status == AWSIotMqttClientStatus.Connected) {
                        System.out.println("Keep Alive");
                        System.out.println(mqttManager.getKeepAlive());
                        subscribeIoT();

                    } else if (status == AWSIotMqttClientStatus.Reconnecting) {
                        if (throwable != null) {
                            System.out.println("Connection error." + throwable);

                            mIIoTServiceListener.IoTConnectionFail(null);
                        }

                    } else if (status == AWSIotMqttClientStatus.ConnectionLost) {
                        if (throwable != null) {
                            System.out.println("Connection error." + throwable);
                            throwable.printStackTrace();

                            mIIoTServiceListener.IoTConnectionFail(null);
                        }

                    } else {

                    }
                }
            });
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "executeIoTStatusTask", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }
    }

    private void subscribeIoT() {
        try {
            mqttManager.subscribeToTopic(ConfigInfo.SUBSCRIBE_TOPIC + IoTCertification.thingName, AWSIotMqttQos.QOS0,
                    new AWSIotMqttNewMessageCallback() {
                        @Override
                        public void onMessageArrived(final String topic, final byte[] data) {
                            try {
                                String message = new String(data, "UTF-8");
                                mIIoTServiceListener.SubscribeMessageReceive(new IoTServiceEvent(message));
                            } catch (UnsupportedEncodingException e) {
                                System.out.println("Message encoding error."+e);
                            }
                        }
                    });
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "subscribeIoT", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }
    }
    public void publishIoT(String msg) throws Exception {
        try {
            mqttManager.publishString(msg, ConfigInfo.PUBLISH_TOPIC + IoTCertification.thingName, AWSIotMqttQos.QOS0);
        } catch(Exception ex) {
            throw new Exception("<IoTManager-publishIoT> : " + ex.getMessage());
        }
    }
}
