package com.eibiz.imediaplayer.utility;

public class LOG_TYPE {
    public static final String INFO = "INFO";
    public static final String IO_ERROR = "IO_ERROR"; // IO
    public static final String INTERNAL_ERROR = "INTERNAL_ERROR"; // app itself
    public static final String NETWORK = "NETWORK"; // network
}
