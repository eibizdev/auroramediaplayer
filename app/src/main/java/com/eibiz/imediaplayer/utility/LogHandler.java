package com.eibiz.imediaplayer.utility;

import com.eibiz.imediaplayer.SignageSystem;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;



public class LogHandler {
    private static String LOG_PATH = SignageSystem.backup_storagePath + "log" + File.separator;
    private static String LOG_FILE = "";
    private static DateFormat DATE_FORMAT__LogFileName = new SimpleDateFormat("yyyy_MM_dd", Locale.US);
    private static DateFormat DATE_FORMAT__WriteLog = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss", Locale.US);



    public static void writeLog(String className, String functionName, String logType, String description) {
        LOG_FILE = LOG_PATH + "log_" + DATE_FORMAT__LogFileName.format(new Date()) + ".log";
        File logFile = new File(LOG_FILE);
        if (!logFile.exists()) {
            createLogFile(logFile);
        }

        try {
            BufferedWriter buffWr = new BufferedWriter(new FileWriter(logFile, true));
            buffWr.append(DATE_FORMAT__WriteLog.format(new Date()));
            buffWr.append("|");
            buffWr.append(className);
            buffWr.append("|");
            buffWr.append(functionName);
            buffWr.append("|");
            buffWr.append(logType);
            buffWr.append("|");
            buffWr.append(description);
            buffWr.append("|");
            buffWr.newLine();
            buffWr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createLogFile(File logFile) {
        try {
            logFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void RemoveOldLogFile() {
        File logPath = new File(LOG_PATH);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -30);
        Date LogExpiryDate = calendar.getTime();
        //System.out.println(">>>>\n:Expiry Date: " + LogExpiryDate.toString());
        if (logPath.exists()) {
            for (File file : logPath.listFiles()) {
                if (file.getName().contains("log")) {
                    String strFileLoggedDate = file.getName().substring(4, (4+10));
                    try {
                        Date dateFileLoggedDate = DATE_FORMAT__LogFileName.parse(strFileLoggedDate);
                        if (dateFileLoggedDate.before(LogExpiryDate)) {
                            file.delete();
                        }
                    } catch (ParseException e) {
                        file.delete();
                        e.printStackTrace();
                    }
                }
            }
        } else {
            logPath.mkdir();
        }
    }
}
