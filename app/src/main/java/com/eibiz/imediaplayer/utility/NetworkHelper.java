package com.eibiz.imediaplayer.utility;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

public class NetworkHelper {
    private static final String ThisClassName = NetworkHelper.class.getSimpleName();
    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> enumNif = NetworkInterface.getNetworkInterfaces(); enumNif.hasMoreElements();) {
                NetworkInterface nif = enumNif.nextElement();
                for (Enumeration<InetAddress> enumInetAddr = nif.getInetAddresses(); enumInetAddr.hasMoreElements();) {
                    InetAddress inetAddr = enumInetAddr.nextElement();
                    if (!inetAddr.isLoopbackAddress() && inetAddr instanceof Inet4Address) {
                        return inetAddr.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            LogHandler.writeLog(ThisClassName, "getLocalIpAddress", LOG_TYPE.INTERNAL_ERROR , ex.toString());
            return "";
        }
        return "";
    }

    public static String getMACAddress(String device) {
        try {
            Enumeration<NetworkInterface> enumeratNetworkInterface = NetworkInterface.getNetworkInterfaces();
            List<NetworkInterface> lstNIF = Collections.list(enumeratNetworkInterface);
            for (NetworkInterface nif : lstNIF) {
                if (nif.getName().equalsIgnoreCase(device)) {
                    byte[] bytesMAC = nif.getHardwareAddress();
                    StringBuilder strbdrMACAddr = new StringBuilder();
                    int b_count = 0;
                    for (byte b : bytesMAC) {
                        b_count++;
                        int buff = b & 0xFF;
                        if (buff <= 0xF) {
                            strbdrMACAddr.append(0);
                        }
                        strbdrMACAddr.append(Integer.toHexString(buff));
                        if (b_count != bytesMAC.length) {
                            strbdrMACAddr.append(":");
                        }
                    }
                    return strbdrMACAddr.toString();
                }
            }
        } catch (Exception ex) {
            LogHandler.writeLog(ThisClassName, "getMACAddress", LOG_TYPE.INTERNAL_ERROR, ex.toString());
            return "";
        }
        return "";
    }
}
