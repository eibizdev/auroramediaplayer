package com.eibiz.imediaplayer.utility;

import com.eibiz.imediaplayer.model.ProfileSchedule;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateHelper {
    private static final String ThisClassName = NetworkHelper.class.getSimpleName();
    //        SimpleDateFormat formatter_date_in = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");
//        SimpleDateFormat formatter_date_out = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
//        SimpleDateFormat formatter_time = new SimpleDateFormat("HH:mm:ss");

    public static Date convertStringToUTC(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(tz);
        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }



    public static String convertDateToString(Date date) {
        TimeZone tz = TimeZone.getTimeZone(getCurrentTimeZone());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static String convertToCurrentTimeZone(String Date) {
        String converted_date = "";
        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date date = utcFormat.parse(Date);

            DateFormat currentTFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            currentTFormat.setTimeZone(TimeZone.getTimeZone(getCurrentTimeZone()));

            converted_date =  currentTFormat.format(date);
        }catch (Exception e){ e.printStackTrace();}

        return converted_date;
    }

    public static String convertToUTC (String Date) {
        String converted_date = "";
        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            utcFormat.setTimeZone(TimeZone.getTimeZone(getCurrentTimeZone()));

            Date date = utcFormat.parse(Date);

            DateFormat currentTFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            currentTFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            converted_date =  currentTFormat.format(date);
        }catch (Exception e){ e.printStackTrace();}

        return converted_date;
    }


    //get the current time zone
    private static String getCurrentTimeZone(){
        TimeZone tz = Calendar.getInstance().getTimeZone();
        System.out.println(tz.getDisplayName());
        return tz.getID();
    }

    public static String convertFreqSchedule(int freqSch) {
        String scheduleType = "";
        switch (freqSch) {
            case 4:
                scheduleType = ProfileSchedule.Sch_Type.Daily;
                break;
            case 8:
                scheduleType = ProfileSchedule.Sch_Type.Weekly;
                break;
            case 16:
                scheduleType = ProfileSchedule.Sch_Type.Monthly;
                break;
        }
        return scheduleType;
    }

    public static List<String> convertFreqIntervalForWeekly(int freqInterval) {
        List<String> week = new ArrayList<String>();
        List<Integer> weekInt = new ArrayList<Integer>(Arrays.asList(1,2,4,8,16,32,64));
        for(int i = 0; i < weekInt.size(); i++) {
            String weekType = "";
            switch ((freqInterval & weekInt.get(i))) {
                case 1 :
                    weekType = ProfileSchedule.Week_Type.Sunday;
                    break;
                case 2 :
                    weekType = ProfileSchedule.Week_Type.Monday;
                    break;
                case 4 :
                    weekType = ProfileSchedule.Week_Type.Tuesday;
                    break;
                case 8 :
                    weekType = ProfileSchedule.Week_Type.Wednesday;
                    break;
                case 16 :
                    weekType = ProfileSchedule.Week_Type.Thursday;
                    break;
                case 32 :
                    weekType = ProfileSchedule.Week_Type.Friday;
                    break;
                case 64 :
                    weekType = ProfileSchedule.Week_Type.Saturday;
                    break;
            }
            if(!weekType.equals(""))
                week.add(weekType);
        }
        return week;
    }

    public static List<Integer> convertFreqIntervalForMonthly(int freqInterval) {
        List<Integer> month = new ArrayList<>();
        List<Integer> monthInt = new ArrayList<Integer>(Arrays.asList(1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,2097152,4194304,8388608,16777216,33554432,67108864,134217728,268435456,536870912,1073741824,-2147483648));
        for(int i = 0; i < monthInt.size(); i++) {
            int monthDate = 0;
            if((freqInterval & monthInt.get(i)) == monthInt.get(i)) {
                if(monthInt.get(i) < 0) {
                    monthDate = -1;
                } else {
                    monthDate = i + 1;
                }
            }

            if(monthDate != 0)
                month.add(monthDate);
        }
        return month;
    }
}
