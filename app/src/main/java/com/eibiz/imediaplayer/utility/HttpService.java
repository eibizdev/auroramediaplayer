package com.eibiz.imediaplayer.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

public class HttpService {
    private static final String ThisClassName = HttpService.class.getSimpleName();

    private HttpURLConnection httpURLConnection = null;

    public HttpService(String urlStr, String method) {
        try {
            URL url = new URL(urlStr);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod(method);
            httpURLConnection.setConnectTimeout(10000);
        } catch (MalformedURLException e) {
            LogHandler.writeLog(ThisClassName, "Constructor", LOG_TYPE.NETWORK + ":" + "MalformedURLException" , e.getMessage());
        } catch (IOException e) {
            LogHandler.writeLog(ThisClassName, "Constructor", LOG_TYPE.NETWORK + ":" + "IOException" , e.getMessage());
        }
    }

    public boolean sendRequest(String strData) {
        if (httpURLConnection == null)
            return false;

        if(strData.compareTo("") != 0) {
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setDoOutput(true);
        }

        try {
            httpURLConnection.connect();

            if(strData.compareTo("") != 0) {
                DataOutputStream dataOutStm = new DataOutputStream(httpURLConnection.getOutputStream());
                dataOutStm.writeBytes(strData);
                dataOutStm.flush();
                dataOutStm.close();
            }

            return true;
        } catch (SocketTimeoutException e) {
            LogHandler.writeLog(ThisClassName, "sendRequest", LOG_TYPE.NETWORK + ":" + "SocketTimeoutException" , e.getMessage());

            return false;
        } catch (IOException e) {
            LogHandler.writeLog(ThisClassName, "sendRequest", LOG_TYPE.NETWORK + ":" + "IOException" , e.getMessage());

            return false;
        }
    }

    public String getResponse() {
        String strJSONResponse = "";

        if (httpURLConnection == null) {
            strJSONResponse = "error";
            return strJSONResponse;
        }

        int httpStatusCode = 0;
        StringBuilder strBdrJSONResponse = new StringBuilder();
        try {
            httpStatusCode = httpURLConnection.getResponseCode();
            if (httpStatusCode == 200 || httpStatusCode == 201) {
                InputStream is = httpURLConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                while ((line = reader.readLine()) != null) {
                    strBdrJSONResponse.append(line);
                }

                strJSONResponse = strBdrJSONResponse.toString();
            } else {
                LogHandler.writeLog(ThisClassName, "getResponse", LOG_TYPE.NETWORK,"response code" + ":" + Integer.toString(httpStatusCode));
                strJSONResponse = "error";
            }
        } catch (IOException e) {
            LogHandler.writeLog(ThisClassName, "getResponse", LOG_TYPE.NETWORK + ":" + "IOException" , e.getMessage());
            strJSONResponse = "error";
        }

        httpURLConnection.disconnect();

        return strJSONResponse;
    }
}

