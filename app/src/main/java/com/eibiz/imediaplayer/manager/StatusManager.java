package com.eibiz.imediaplayer.manager;

import android.os.AsyncTask;
import android.os.Build;

import com.eibiz.imediaplayer.MainActivity;
import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.Player;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.model.PublishProfile;
import com.eibiz.imediaplayer.service.PlayerService;
import com.eibiz.imediaplayer.service.ProfileService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

public class StatusManager {
    private static final String ThisClassName = StatusManager.class.getSimpleName();
    private static int tryDownloadCount = 0;
    private static boolean calledCreatePlayerAPISuccess = false;
    private static boolean downloadExecute = false;
    private static final int maxDelay = 30000;
    private static boolean displayQR = false;
    // Status EVENT.
    public class StatusEvent extends EventObject {
        public StatusEvent(Object source) {
            super(source);
        }
    }
    public interface IStatusListener {
        void DownloadComplete(StatusEvent e);
        void SettingComplete(StatusEvent e, String path);
        void DisplayQRCode(StatusEvent e);
        void CompleteRegistration(StatusEvent e);
    }
    public static IStatusListener mIStatusListener = null;
    public void setIStatusListener(IStatusListener pIStatusListener) {
        mIStatusListener = pIStatusListener;
    }

    private StatusAsyncTask statusAsyncTask = null;
    //private static List<ProfilePlaylistMedia> downloadList = new ArrayList<ProfilePlaylistMedia>();
    private static List<String> removeList = new ArrayList<String>();

    private DownloadManager.IDownloadListener mIDownloadListener = new DownloadManager.IDownloadListener() {
        @Override
        public void DownloadFinishedWithSuccessful(DownloadManager.DownloadEvent e) {
            try {
                SignageSystem.saveJson();
                SignageSystem.loadJson();
                // delete content which unused
                deleteAllUnusedContent(removeList);

                // call API to update work status
                SignageSystem.local.curPublishProfile.status = PublishProfile.Work_Status.Complete;
                SignageSystem.local.curPublishProfile.reason = PublishProfile.Work_Reason.None;
                LoopToSendUpdateWorkStatus();

                SignageSystem.server.clearData();               // clear ProfileSchedule n ProfilePlaylistMedias

                SignageSystem.local.player.status = Player.Player_Status.Online;
                SignageSystem.local.player.reason = Player.Player_Reason.None;
                SignageSystem.local.clearData();                // clear PublishProfile
                SignageSystem.local.transferLogs.clear(); // local transferLog clear n save
                SignageSystem.saveTransferLogJson();

                mIStatusListener.DownloadComplete(null);
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "UpdateForSuccessfulDownload", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void DownloadFinishedWithFailure(DownloadManager.DownloadEvent e) {
            if (tryDownloadCount < 7) {
                tryDownloadCount++;
                SignageSystem.local.player.status = Player.Player_Status.Online;
                return;
            }

            try {
                // delete content which already downloaded
                deleteAllDownloadedContent(SignageSystem.local.transferLogs);

                // call API to update Player Status & work status
                SignageSystem.local.curPublishProfile.status = PublishProfile.Work_Status.Error;
                SignageSystem.local.curPublishProfile.reason = PublishProfile.Work_Reason.FailToDownloadContent;
                LoopToSendUpdateWorkStatus();

                SignageSystem.server.clearData();               // clear ProfileSchedule n ProfilePlaylistMedias

                SignageSystem.local.player.status = Player.Player_Status.Online;
                SignageSystem.local.clearData();                // clear PublishProfile
                SignageSystem.local.transferLogs.clear();
                SignageSystem.saveTransferLogJson();
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "UpdateForFailureDownload", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void DownloadCancelled(DownloadManager.DownloadEvent e) {
            try {
                // delete content which already downloaded
                deleteAllDownloadedContent(SignageSystem.local.transferLogs);

                // call API to update Player Status & work status
                SignageSystem.local.curPublishProfile.status = PublishProfile.Work_Status.Cancel;
                SignageSystem.local.curPublishProfile.reason = PublishProfile.Work_Reason.ServerRequested;
                LoopToSendUpdateWorkStatus();

                SignageSystem.server.clearData();               // clear ProfileSchedule n ProfilePlaylistMedias

                SignageSystem.local.player.status = Player.Player_Status.Online;
                SignageSystem.local.clearData();                // clear PublishProfile
                SignageSystem.local.transferLogs.clear();
                SignageSystem.saveTransferLogJson();
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "DownloadCancelled", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };

    public StatusManager() {
        MainActivity.downloadManager.setIDownloadListener(mIDownloadListener);
        statusAsyncTask = new StatusAsyncTask();
        if (Build.VERSION.SDK_INT >= 11)
            statusAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            statusAsyncTask.execute();
    }

    private static int getRandomInt(int max, int min) {
        return ((int)(Math.random()*(max - min)) + min);
    }

    private void LoopToSendUpdateWorkStatus() {
        while(true) {
            try {
                if (ProfileService.updateWorkStatus()) {
                    break;
                } else {
                    Thread.sleep(1000);
                }
            } catch (InterruptedException ex) { ex.printStackTrace(); }
        }
        downloadExecute = false;
    }

    private static class StatusAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            if(SignageSystem.local.deviceSetting.smartDelay == -1) {
                SignageSystem.local.deviceSetting.smartDelay = getRandomInt(300, 0) * 100;
                SignageSystem.saveDeviceSetting();
            }
            try {
                System.out.println("1************************************Sleep : " + SignageSystem.local.deviceSetting.smartDelay);
                Thread.sleep(SignageSystem.local.deviceSetting.smartDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    if(SignageSystem.local.player.idPlayer == 0 || SignageSystem.local.player.idCompany == 0) {                              // device previous player data is not exist
                        SignageSystem.server.player = PlayerService.createPlayer();				// sending API createPlayer to server.

                        if (SignageSystem.server.player != null
                                && SignageSystem.server.player.idPlayer != 0 && SignageSystem.server.player.idCompany != 0) {                 // device success to get data from server
                            SignageSystem.local.player.idCompany = SignageSystem.server.player.idCompany;
                            SignageSystem.local.player.idPlayer = SignageSystem.server.player.idPlayer;
                            SignageSystem.local.player.name = SignageSystem.server.player.name;
                            calledCreatePlayerAPISuccess = true;
                            SignageSystem.local.deviceSetting.smartDelay = maxDelay;            // 30 sec

                            if(displayQR) {
                                mIStatusListener.CompleteRegistration(null);
                                displayQR = false;
                            }

                        } else {                                                                // device fail to get data from server
                            if(SignageSystem.server.player.idPlayer != 0 && SignageSystem.server.player.idCompany == 0) {
                                SignageSystem.local.deviceSetting.smartDelay = maxDelay;        // success to assign in server but not assigned company yet

                                mIStatusListener.DisplayQRCode(null);
                                displayQR = true;
                            } else {                                                            // fail to assign in server
                                SignageSystem.local.deviceSetting.smartDelay = getRandomInt(300, 0) * 100;
                                SignageSystem.saveDeviceSettingJson();		                        // generate SmartDelay Time
                            }
                        }
                    } else {                                                                    // device previous player data is exist
                        if(SignageSystem.local.player.status.equals(Player.Player_Status.Offline)) {               // device start
                            SignageSystem.local.deviceSetting.smartDelay = 0;
                        } else {                                                                // device run already
                            SignageSystem.local.deviceSetting.smartDelay = maxDelay;            // 30 sec
                        }
                    }

                    if(SignageSystem.local.player.status.equals(Player.Player_Status.Offline)) {
                        SignageSystem.local.player.status = Player.Player_Status.Online;
                        SignageSystem.local.player.reason = Player.Player_Reason.None;
                    }
                    else if (SignageSystem.local.player.status.equals(Player.Player_Status.Online)) {
                        if((SignageSystem.server.publishProfile.status.equals(PublishProfile.Work_Status.Init) || SignageSystem.server.publishProfile.status.equals(PublishProfile.Work_Status.Execute))
                        && SignageSystem.server.playlistMedias.size() > 0 && (downloadExecute == false|| (tryDownloadCount > 0 && tryDownloadCount <= 7))) {    // In Server, publish && has playlistmedias data from server
                            if(downloadExecute == false)
                                tryDownloadCount = 0;   //reset
                            SignageSystem.local.curPublishProfile.idPlayerPublishHistory = SignageSystem.server.publishProfile.idPlayerPublishHistory;
                            SignageSystem.local.curPublishProfile.status = PublishProfile.Work_Status.Execute;
                            SignageSystem.local.curPublishProfile.reason = PublishProfile.Work_Reason.Downloading;

                            if(ProfileService.updateWorkStatus()) { // success to updateWorkStatus in Server
                                downloadAll();
                            } else {                                // fail to updateWorkStatus in Server
                                System.out.println("Fail to updateWorkStatus in Server.");
                            }
                        }
                    } else if (SignageSystem.local.player.status.equals(Player.Player_Status.Error)) {  }
                } catch (Exception ex) {
                    LogHandler.writeLog(ThisClassName, "StatusAsyncTask", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                } finally {
                    try {
                        //Thread.sleep(1000);
                        Thread.sleep(SignageSystem.local.deviceSetting.smartDelay);
                        if(!calledCreatePlayerAPISuccess) {
                            PlayerService.updatePlayerStatus();
                            if(SignageSystem.local.curPublishProfile.idPlayerPublishHistory != 0 && SignageSystem.local.curPublishProfile.status.compareTo(PublishProfile.Work_Status.Cancel) == 0) {     // previous publish history exist && that publish history is cancelled
                                // cancel downloading process
                                MainActivity.downloadManager.cancel();
                            }
                            if(SignageSystem.server.publishProfile.idPlayerPublishHistory != 0 && (SignageSystem.server.publishProfile.status.equals(PublishProfile.Work_Status.Init) || SignageSystem.server.publishProfile.status.equals(PublishProfile.Work_Status.Execute)) && SignageSystem.local.curPublishProfile.idPlayerPublishHistory != SignageSystem.server.publishProfile.idPlayerPublishHistory) {       // new publish history exist from server
                                // get publishProfile
                                SignageSystem.server.clearData();               // clear ProfileSchedule n ProfilePlaylistMedias
                                ProfileService.getPublishProfile(SignageSystem.local.player.idCompany, SignageSystem.local.player.idPlayer, SignageSystem.server.publishProfile.idPlayerPublishHistory);
                            }
                        } else {
                            calledCreatePlayerAPISuccess = false;
                        }
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
    private static void downloadAll() {
        try {
            downloadExecute = true;
            generateRemoveList();

            MainActivity.downloadManager.startDownloadThread(SignageSystem.server.playlistMedias);
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "downloadAll", LOG_TYPE.INTERNAL_ERROR , ex.getMessage());
        }
    }
    private static void generateRemoveList() throws Exception {
        try {
            removeList.clear();
            // remove list
            if (SignageSystem.local.playlistMedias != null) {
                for (int i = 0; i < SignageSystem.local.playlistMedias.size(); i++) {
                    ProfilePlaylistMedia tmpCurrentPPM = SignageSystem.local.playlistMedias.get(i);
                    String filePath = SignageSystem.storagePath + SignageSystem.MediaPath + tmpCurrentPPM.mediaName;
                    if(!removeList.contains(filePath)) {
                        removeList.add(filePath);
                    }
                }
                for (int j = 0; j < SignageSystem.server.playlistMedias.size(); j++) {
                    ProfilePlaylistMedia tmpServerPPM = SignageSystem.server.playlistMedias.get(j);
                    String filePath = SignageSystem.storagePath + SignageSystem.MediaPath + tmpServerPPM.mediaName;
                    if(removeList.contains(filePath)) {
                        removeList.remove(filePath);
                    }
                }
            }
        } catch (Exception ex) {
            throw new Exception("<StatusManager-generateRemoveList[step01]> : " + ex.getMessage());
        }
    }
    private static void deleteAllDownloadedContent(List<ProfilePlaylistMedia> list) {
        for(int i = 0; i < list.size(); i++) {
            ProfilePlaylistMedia tmpPPM = list.get(i);
            File file = new File(SignageSystem.storagePath + SignageSystem.MediaPath + tmpPPM.mediaName);
            if (file.exists()) {
                file.delete();
            }
        }
    }
    private static void deleteAllUnusedContent(List<String> list) {
        for(int i = 0; i < list.size(); i++) {
            String filePath = list.get(i);
            File file = new File(filePath);
            if(file.exists()) {
                file.delete();
            }
        }
    }
}
