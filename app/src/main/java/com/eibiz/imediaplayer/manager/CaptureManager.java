package com.eibiz.imediaplayer.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Base64;
import android.view.View;

import com.eibiz.imediaplayer.MainActivity;
import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.service.CaptureService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class CaptureManager {
    private static final String ThisClassName = DownloadManager.class.getSimpleName();

    private ScreenCaptureAsyncTask captureAsyncTask = null;

    public CaptureManager() {
        captureAsyncTask = new ScreenCaptureAsyncTask();
        captureAsyncTask.IsCancelled = false;

        if (Build.VERSION.SDK_INT >= 11)
            captureAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            captureAsyncTask.execute();
    }

    private class ScreenCaptureAsyncTask extends AsyncTask<Void, Void, Void> {
        boolean IsCancelled = false;

        @Override
        protected void onCancelled() {
            super.onCancelled();
            IsCancelled = true;
            captureAsyncTask.cancel(IsCancelled);
        }

        @Override
        protected Void doInBackground(Void... params) {

            while (!IsCancelled) {
                try {
                    Thread.sleep(SignageSystem.Screenshot_Interval * 1000);
                } catch (InterruptedException ex) {
                    LogHandler.writeLog(ThisClassName, "doInBackground", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                }

                File directory = new File(SignageSystem.backup_storagePath + SignageSystem.CapturePath);
                if (!directory.exists()) {
                    directory.mkdir();
                }
                String path = SignageSystem.backup_storagePath + SignageSystem.CapturePath + "player_capture.png";
                String resize_path = SignageSystem.backup_storagePath + SignageSystem.CapturePath + "resize.png";
                //getScreenShot(ScreenManager.curDisplayScreen);

                try {
                    // screenshot
                    Process sh = Runtime.getRuntime().exec("su", null,null);
                    OutputStream os = sh.getOutputStream();
                    os.write(("/system/bin/screencap -p " + path).getBytes("ASCII"));
                    os.flush();
                    os.close();
                    sh.waitFor();
                    // resize as bitmap & save as file
                    Bitmap bmp = resizeBitmap(path, SignageSystem.fullScreenWidth / 10, SignageSystem.fullScreenHeight / 10);

                    File file = new File(resize_path);
                    FileOutputStream fOut = new FileOutputStream(file);

                    bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                    fOut.flush();
                    fOut.close();
                    // convert image to base64
                    Bitmap bm = BitmapFactory.decodeFile(resize_path);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

                    // send to API
                    CaptureService.uploadScreenCapture(encodedImage);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                System.out.println("Screen capture done!: ");
            }
            return null;
        }
    }

    public Bitmap resizeBitmap(String photoPath, int targetW, int targetH) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        }

        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true; //Deprecated API 21

        return BitmapFactory.decodeFile(photoPath, bmOptions);
    }
}
