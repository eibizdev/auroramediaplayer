package com.eibiz.imediaplayer.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.eibiz.imediaplayer.DisplayScreen;
import com.eibiz.imediaplayer.R;
import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ConfigInfo;
import com.eibiz.imediaplayer.model.ProfileSchedule;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;
import com.eibiz.imediaplayer.utility.NetworkHelper;

import net.glxn.qrgen.android.QRCode;

public class ScreenManager {
    private static Context context = null;
    private static final String ThisClassName = ScreenManager.class.getSimpleName();
    public static AppCompatTextView txtVwAndroidPlayerStatus = null;
    public static AppCompatTextView txtVwAndroidVersionName = null;
    public static AppCompatTextView txtVwAndroidMacAddress = null;
    public static PlaylistManager playlistManager = null;
    public static DisplayScreen curDisplayScreen = null;

    public ScreenManager(Context pContext) {
        context = pContext;
        gettingFullScreenSize();
    }

    private void gettingFullScreenSize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        if (Build.VERSION.SDK_INT >= 17) {
            display.getRealMetrics(displayMetrics);
        } else if (Build.VERSION.SDK_INT >= 14) {
            display.getMetrics(displayMetrics);
        } else {
            display.getMetrics(displayMetrics);
        }
        SignageSystem.fullScreenWidth = displayMetrics.widthPixels;
        SignageSystem.fullScreenHeight = displayMetrics.heightPixels;
        SignageSystem.local.player.screenDimension = SignageSystem.fullScreenWidth  + " x " + SignageSystem.fullScreenHeight;
        // calculate screenRitio
        int factor = greatestCommonFactor( SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight);
        int widthRatio = SignageSystem.fullScreenWidth / factor;
        int heightRatio = SignageSystem.fullScreenHeight / factor;
        SignageSystem.local.player.screenRatio = widthRatio + " : " + heightRatio;
    }

    public static int greatestCommonFactor(int width, int height) {
        return (height == 0) ? width : greatestCommonFactor(height, width % height);
    }

    public boolean drawIntro() {
        Drawable drawableLogo = context.getResources().getDrawable(R.drawable.ic_launcher_i_media);
        int drawableLogo_Width = drawableLogo.getIntrinsicWidth();
        int drawableLogo_Height = drawableLogo.getIntrinsicHeight();
        ImageButton imgBtnLogo = new ImageButton(context);
        imgBtnLogo.setBackgroundResource(R.drawable.imediaplayer_512);

        int centerPortion_Height = Math.round(2 * SignageSystem.fullScreenHeight / 4);
        int imgBtnLogo_y = SignageSystem.fullScreenHeight / 4;

        int overallViewSizeFactor = 2;

        int imgBtnLogo_Height = Math.round(1 * centerPortion_Height / overallViewSizeFactor);
        int imgBtnLogo_Width = Math.round(imgBtnLogo_Height * drawableLogo_Width / drawableLogo_Height);
        int imgBtnLogo_x = Math.round(SignageSystem.fullScreenWidth / 2) - Math.round(imgBtnLogo_Width / 2);

        FrameLayout.LayoutParams imgBtnLogo_LayoutParams = new FrameLayout.LayoutParams(imgBtnLogo_Width, imgBtnLogo_Height);
        imgBtnLogo_LayoutParams.leftMargin = imgBtnLogo_x;
        imgBtnLogo_LayoutParams.topMargin = imgBtnLogo_y;
        imgBtnLogo.setLayoutParams(imgBtnLogo_LayoutParams);

        ProgressBar progBarInitScreen = new ProgressBar(context);
        int progBarInitScreen_Width = Math.round(imgBtnLogo_Height / 2);
        int progBarInitScreen_Height = progBarInitScreen_Width;
        int progBarInitScreen_x = Math.round(SignageSystem.fullScreenWidth / 2) - Math.round(progBarInitScreen_Width / 2);
        int progBarInitScreen_y = imgBtnLogo_y + imgBtnLogo_Height + Math.round(progBarInitScreen_Height / 8);
        progBarInitScreen.setIndeterminate(true);
        progBarInitScreen.setVisibility(View.VISIBLE);
        progBarInitScreen.getIndeterminateDrawable().setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.MULTIPLY);

        FrameLayout.LayoutParams progBarInitScreen_LayoutParams = new FrameLayout.LayoutParams(progBarInitScreen_Width, progBarInitScreen_Height);
        progBarInitScreen_LayoutParams.leftMargin = progBarInitScreen_x;
        progBarInitScreen_LayoutParams.topMargin = progBarInitScreen_y;
        progBarInitScreen.setLayoutParams(progBarInitScreen_LayoutParams);

        txtVwAndroidPlayerStatus = new AppCompatTextView(context);
        txtVwAndroidPlayerStatus.setTextColor(Color.WHITE);
        txtVwAndroidPlayerStatus.setTextSize(imgBtnLogo_Height / 20);
        int txtVwAndroidPlayerStatus_y = progBarInitScreen_y + progBarInitScreen_Height + (progBarInitScreen_y - (imgBtnLogo_y + imgBtnLogo_Height));
        if (progBarInitScreen.getVisibility() == View.GONE) {
            txtVwAndroidPlayerStatus_y = progBarInitScreen_y;
        }
        txtVwAndroidPlayerStatus.setGravity(Gravity.CENTER_HORIZONTAL);

        FrameLayout.LayoutParams txtVwAndroidPlayerStatus_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtVwAndroidPlayerStatus_LayoutParams.leftMargin = 0;
        txtVwAndroidPlayerStatus_LayoutParams.topMargin = txtVwAndroidPlayerStatus_y;
        txtVwAndroidPlayerStatus.setLayoutParams(txtVwAndroidPlayerStatus_LayoutParams);

        txtVwAndroidVersionName = new AppCompatTextView(context);
        txtVwAndroidVersionName.setTextColor(Color.WHITE);
        txtVwAndroidVersionName.setTextSize(imgBtnLogo_Height / 22);
        int txtVwAndroidVersionName_x = SignageSystem.fullScreenWidth * 27 / 32;
        int txtVwAndroidVersionName_y = SignageSystem.fullScreenHeight * 15 / 16;

        FrameLayout.LayoutParams txtVwAndroidVersionName_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtVwAndroidVersionName_LayoutParams.leftMargin = txtVwAndroidVersionName_x;
        txtVwAndroidVersionName_LayoutParams.topMargin = txtVwAndroidVersionName_y;
        txtVwAndroidVersionName.setLayoutParams(txtVwAndroidVersionName_LayoutParams);

        txtVwAndroidMacAddress = new AppCompatTextView(context);
        txtVwAndroidMacAddress.setTextColor(Color.WHITE);
        txtVwAndroidMacAddress.setTextSize(imgBtnLogo_Height / 22);
        int txtVwAndroidMacAddress_x = SignageSystem.fullScreenHeight * 27 / 32;
        int txtVwAndroidMacAddress_y = SignageSystem.fullScreenHeight * 15 / 16;

        FrameLayout.LayoutParams txtVwAndroidMacAddress_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        txtVwAndroidMacAddress_LayoutParams.leftMargin = 30;
        txtVwAndroidMacAddress_LayoutParams.topMargin = txtVwAndroidMacAddress_y;
        txtVwAndroidMacAddress.setLayoutParams(txtVwAndroidMacAddress_LayoutParams);

        FrameLayout introLayout = new FrameLayout(context);

        FrameLayout.LayoutParams introLayout_LayoutParams = new FrameLayout.LayoutParams(SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight);
        introLayout_LayoutParams.leftMargin = 0;
        introLayout_LayoutParams.topMargin = 0;
        introLayout.setLayoutParams(introLayout_LayoutParams);

        introLayout.addView(imgBtnLogo);
        introLayout.addView(progBarInitScreen);
        introLayout.addView(txtVwAndroidPlayerStatus);
        introLayout.addView(txtVwAndroidVersionName);
        introLayout.addView(txtVwAndroidMacAddress);

        DisplayScreen introScreen = new DisplayScreen(context);

        FrameLayout.LayoutParams introScreen_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        introScreen_LayoutParams.leftMargin = 0;
        introScreen_LayoutParams.topMargin = 0;
        introScreen.setLayoutParams(introScreen_LayoutParams);

        introScreen.addView(introLayout);

        // setup the curDisplayScreen to be Full-Screen with Black-BackgroundColor:
        curDisplayScreen = new DisplayScreen(context);
        curDisplayScreen.setBackgroundColor(Color.BLACK);
        curDisplayScreen.setLeft(0);
        curDisplayScreen.setTop(0);
        curDisplayScreen.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        curDisplayScreen.addView(introScreen);
        return true;
    }

    public static boolean drawEmpty(ProfileSchedule ps) {
        try {
            if (playlistManager != null)
                playlistManager.clearViews();
            curDisplayScreen.removeAllViews();

            AppCompatTextView versionName = new AppCompatTextView(context);
            versionName.setTextColor(Color.WHITE);
            versionName.setTextSize(Math.round(1 * Math.round(2 * SignageSystem.fullScreenHeight / 4) / 2) / 22);
            int txtVwAndroidVersionName_x = SignageSystem.fullScreenWidth * 27 / 32;
            int txtVwAndroidVersionName_y = SignageSystem.fullScreenHeight * 15 / 16;

            FrameLayout.LayoutParams txtVwAndroidVersionName_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            txtVwAndroidVersionName_LayoutParams.leftMargin = txtVwAndroidVersionName_x;
            txtVwAndroidVersionName_LayoutParams.topMargin = txtVwAndroidVersionName_y;
            versionName.setLayoutParams(txtVwAndroidVersionName_LayoutParams);
            versionName.setText("version: " + SignageSystem.version);

            AppCompatTextView macAddress = new AppCompatTextView(context);
            macAddress.setTextColor(Color.WHITE);
            macAddress.setTextSize(Math.round(1 * Math.round(2 * SignageSystem.fullScreenHeight / 4) / 2) / 22);
            int txtVwAndroidMacAddress_x = SignageSystem.fullScreenWidth * 27 / 32;
            int txtVwAndroidMacAddress_y = SignageSystem.fullScreenHeight * 15 / 16;

            FrameLayout.LayoutParams txtVwAndroidMacAddress_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            txtVwAndroidMacAddress_LayoutParams.leftMargin = 30;
            txtVwAndroidMacAddress_LayoutParams.topMargin = txtVwAndroidMacAddress_y;
            macAddress.setLayoutParams(txtVwAndroidMacAddress_LayoutParams);
            macAddress.setText("macAddress: " + NetworkHelper.getMACAddress("wlan0"));

            FrameLayout introLayout = new FrameLayout(context);

            FrameLayout.LayoutParams introLayout_LayoutParams = new FrameLayout.LayoutParams(SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight);
            introLayout_LayoutParams.leftMargin = 0;
            introLayout_LayoutParams.topMargin = 0;
            introLayout.setLayoutParams(introLayout_LayoutParams);

            introLayout.addView(versionName);
            introLayout.addView(macAddress);

            DisplayScreen introScreen = new DisplayScreen(context);

            FrameLayout.LayoutParams introScreen_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            introScreen_LayoutParams.leftMargin = 0;
            introScreen_LayoutParams.topMargin = 0;
            introScreen.setLayoutParams(introScreen_LayoutParams);

            introScreen.addView(introLayout);

            curDisplayScreen.addView(introScreen);

            // send api to display blackscreen via drawEmpty method execute
//            SignageSystem.local.curSchedule = new ProfileSchedule();
//            PlayerService.updatePlayerStatus();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "drawEmpty", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }

        return true;
    }

    public static boolean drawScreen(ProfileSchedule ps) {
        try {
            curDisplayScreen.removeAllViews();

            playlistManager = new PlaylistManager(context, ps.idProfileSchedule);
            playlistManager.setLayoutParams(new ViewGroup.LayoutParams(AbsoluteLayout.LayoutParams.MATCH_PARENT, AbsoluteLayout.LayoutParams.MATCH_PARENT));
            curDisplayScreen.addView(playlistManager);
            playlistManager.startPlaylist();

            // send api to display current playlist via drawScreen method execute
//            SignageSystem.local.curSchedule = ps;
//            PlayerService.updatePlayerStatus();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "drawScreen", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }

        return true;
    }

    public static boolean drawQRCode() {
        try {
            if (playlistManager != null)
                playlistManager.clearViews();
            curDisplayScreen.removeAllViews();
            ///////////////////////////////////////////////////////////////
            AppCompatTextView versionName = new AppCompatTextView(context);
            versionName.setTextColor(Color.WHITE);
            versionName.setTextSize(Math.round(1 * Math.round(2 * SignageSystem.fullScreenHeight / 4) / 2) / 22);
            int txtVwAndroidVersionName_x = SignageSystem.fullScreenWidth * 27 / 32;
            int txtVwAndroidVersionName_y = SignageSystem.fullScreenHeight * 15 / 16;

            FrameLayout.LayoutParams txtVwAndroidVersionName_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            txtVwAndroidVersionName_LayoutParams.leftMargin = txtVwAndroidVersionName_x;
            txtVwAndroidVersionName_LayoutParams.topMargin = txtVwAndroidVersionName_y;
            versionName.setLayoutParams(txtVwAndroidVersionName_LayoutParams);
            versionName.setText("version: " + SignageSystem.version);
            ///////////////////////////////////////////////////////////////
            AppCompatTextView macAddress = new AppCompatTextView(context);
            macAddress.setTextColor(Color.WHITE);
            macAddress.setTextSize(Math.round(1 * Math.round(2 * SignageSystem.fullScreenHeight / 4) / 2) / 22);
            int txtVwAndroidMacAddress_x = SignageSystem.fullScreenWidth * 27 / 32;
            int txtVwAndroidMacAddress_y = SignageSystem.fullScreenHeight * 15 / 16;

            FrameLayout.LayoutParams txtVwAndroidMacAddress_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            txtVwAndroidMacAddress_LayoutParams.leftMargin = 30;
            txtVwAndroidMacAddress_LayoutParams.topMargin = txtVwAndroidMacAddress_y;
            macAddress.setLayoutParams(txtVwAndroidMacAddress_LayoutParams);
            macAddress.setText("macAddress: " + NetworkHelper.getMACAddress("wlan0"));
            ///////////////////////////////////////////////////////////////
            AppCompatImageView qrImage = new AppCompatImageView(context);
            Bitmap myBitmap = QRCode.from(ConfigInfo.QRCodeServer + NetworkHelper.getMACAddress("wlan0")).bitmap();
            qrImage.setImageBitmap(myBitmap);

            int qrImage_x = SignageSystem.fullScreenWidth / 3;
            int qrImage_y = SignageSystem.fullScreenHeight / 3;
            int qrImageSize = 0;
            if(qrImage_x > qrImage_y)
                qrImageSize = qrImage_y;
            else
                qrImageSize = qrImage_x;

            FrameLayout.LayoutParams qrImage_LayoutParams = new FrameLayout.LayoutParams(qrImageSize, qrImageSize);
            qrImage_LayoutParams.leftMargin = (SignageSystem.fullScreenWidth - qrImageSize) / 2;
            qrImage_LayoutParams.topMargin = (SignageSystem.fullScreenHeight - qrImageSize) / 2;
            qrImage.setLayoutParams(qrImage_LayoutParams);
            ///////////////////////////////////////////////////////////////

            FrameLayout introLayout = new FrameLayout(context);

            FrameLayout.LayoutParams introLayout_LayoutParams = new FrameLayout.LayoutParams(SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight);
            introLayout_LayoutParams.leftMargin = 0;
            introLayout_LayoutParams.topMargin = 0;
            introLayout.setLayoutParams(introLayout_LayoutParams);

            introLayout.addView(versionName);
            introLayout.addView(macAddress);
            introLayout.addView(qrImage);

            DisplayScreen introScreen = new DisplayScreen(context);

            FrameLayout.LayoutParams introScreen_LayoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            introScreen_LayoutParams.leftMargin = 0;
            introScreen_LayoutParams.topMargin = 0;
            introScreen.setLayoutParams(introScreen_LayoutParams);

            introScreen.addView(introLayout);

            curDisplayScreen.addView(introScreen);

        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "drawEmpty", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }

        return true;
    }
}
