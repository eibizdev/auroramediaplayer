package com.eibiz.imediaplayer.manager;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.eibiz.imediaplayer.component.ImagePanel;
import com.eibiz.imediaplayer.component.VideoPanel;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.service.ProfileService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.util.ArrayList;
import java.util.List;

public class PlaylistManager extends FrameLayout {
    private Context context;

    private static final String ThisClassName = PlaylistManager.class.getSimpleName();

    private VideoPanel videoPanel = null;
    private ImagePanel imagePanel = null;
    private int curIndex = -1;
    private List<ProfilePlaylistMedia> ppms = null;

    public PlaylistManager(Context pContext, int idProfileSchedule) {
        super(pContext);
        generateCurrentProfilePlaylistMedias(idProfileSchedule);
        context = pContext;
    }

    private void generateCurrentProfilePlaylistMedias(int idProfileSchedule) {
        ppms = new ArrayList<ProfilePlaylistMedia>();
        ppms.addAll(ProfileService.getProfilePlaylistMedias(idProfileSchedule));
    }

    public void startPlaylist() {
        try {
            playNextItem();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "startPlaylist", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }
    }

    private void playNextItem() throws Exception {
        try {
            curIndex ++;

            if(ppms.size() == 0)
                return;

            if(curIndex == ppms.size())
                curIndex = 0;
            ProfilePlaylistMedia curPPM = ppms.get(curIndex);

            if(curPPM == null)
                return;

            if(curPPM.mediaType.compareTo(ProfilePlaylistMedia.PlaylistMedia_MediaType.Video) == 0) {
                videoPanel = new VideoPanel(context,curPPM);
                videoPanel.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                videoPanel.setVideoPanelListener(mVideoPanelListener);
                addView(videoPanel);
            } else if (curPPM.mediaType.compareTo(ProfilePlaylistMedia.PlaylistMedia_MediaType.Image) == 0) {
                imagePanel = new ImagePanel(context, curPPM);
                imagePanel.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                imagePanel.setImagePanelListener(mImagePanelListener);
                addView(imagePanel);
                imagePanel.playerStart();
            }
        } catch(Exception ex) {
            throw new Exception("<PlaylistManager-playNextItem[step01]> : " + ex.getMessage());
        }
    }

    public void clearViews() {
        try {
            removeAllViews();

            if (videoPanel != null) {
                videoPanel.playerClose();
                videoPanel = null;
            }
            if (imagePanel != null) {
                imagePanel.playerClose();
                imagePanel = null;
            }
            System.gc();
        } catch (Exception ex) {
            LogHandler.writeLog(ThisClassName, "clearViews", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }
    }

    VideoPanel.VideoPanelListener mVideoPanelListener = new VideoPanel.VideoPanelListener() {
        @Override
        public void OnLoaded(VideoPanel.VideoPanelEvent e) {
            try {
                videoPanel.playerStart();
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mVideoPanelListener.OnLoaded", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                clearViews();
                startPlaylist();
            }
        }
        @Override
        public void EndMedia(VideoPanel.VideoPanelEvent e) {
            try{
                clearViews();
                playNextItem();
            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "mVideoPanelListener.EndMedia", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
        @Override
        public void ErrorMedia(VideoPanel.VideoPanelEvent e) {
            try {
                clearViews();
                playNextItem();
            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "mVideoPanelListener.ErrorMedia", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };

    ImagePanel.ImagePanelListener mImagePanelListener = new ImagePanel.ImagePanelListener() {
        @Override
        public void EndMedia(ImagePanel.ImagePanelEvent e) {
            try {
                clearViews();
                playNextItem();

            } catch (Exception ex) {
                LogHandler.writeLog(ThisClassName, "mImagePanelListener.EndMedia", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };
}
