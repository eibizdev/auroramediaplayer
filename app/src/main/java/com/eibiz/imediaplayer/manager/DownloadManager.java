package com.eibiz.imediaplayer.manager;

import android.os.AsyncTask;
import android.os.Build;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ConfigInfo;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.service.ContentService;
import com.eibiz.imediaplayer.service.ProfileService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

public class DownloadManager {
    private static final String ThisClassName = DownloadManager.class.getSimpleName();

    public class DownloadEvent extends EventObject {
        public DownloadEvent(Object source) {
            super(source);
        }
    }
    public interface IDownloadListener {
        void DownloadCancelled(DownloadEvent e);
        void DownloadFinishedWithSuccessful(DownloadEvent e);
        void DownloadFinishedWithFailure(DownloadEvent e);
    }
    public IDownloadListener mIDownloadListener = null;
    public void setIDownloadListener(IDownloadListener pIDownloadListener) {
        mIDownloadListener = pIDownloadListener;
    }

    //public DownloadThread downloadThread = null;
    private int failCountDown = 10;

    private DownloadAsyncTask downloadAsyncTask = null;

    private List<ProfilePlaylistMedia> downloadList = new ArrayList<ProfilePlaylistMedia>();

    public DownloadManager() { }
    public boolean startDownloadThread(List<ProfilePlaylistMedia> pDownloadList) {
        if (pDownloadList.size() == 0)
            return false;

        downloadList.clear();
        downloadList.addAll(pDownloadList);

        failCountDown = 10;
        /*
        downloadThread = new DownloadThread();
        downloadThread.setDaemon(false);
        downloadThread.start();*/

        downloadAsyncTask = new DownloadAsyncTask();
        downloadAsyncTask.IsCancelled = false;

        if (Build.VERSION.SDK_INT >= 11)
            downloadAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            downloadAsyncTask.execute();

        return true;
    }
    public void cancel() {
        downloadAsyncTask.onCancelled();
    }
    private void sendProfilePlaylistMedia(ProfilePlaylistMedia ppm) {
        try {
            if(ConfigInfo.connectionType.equals(ConfigInfo.ConnectionType.RestAPI)) {
                if (ProfileService.updateProfilePlaylistMedia(ppm)) { // success to send info to server
                    downloadList.remove(0);
                }
            } else {
                if(ProfileService.updateProfilePlaylistMediaIoT(ppm)) { // success to send info to server
                    downloadList.remove(0);
                }
            }

        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "sendProfilePlaylistMedia", LOG_TYPE.NETWORK, ex.getMessage());
        }
    }

    private class DownloadAsyncTask extends AsyncTask<Void, Void, Void> {
        private boolean IsCancelled = false;
        private boolean isSuccessFullyDownload = true;

        @Override
        protected void onCancelled() {
            super.onCancelled();
            IsCancelled = true;
            downloadAsyncTask.cancel(IsCancelled);
        }

        @Override
        protected Void doInBackground(Void... params) {
            ProfilePlaylistMedia ppm = null;
            while (!IsCancelled && downloadList.size() > 0) {
                if (failCountDown == 10) {
                    ppm = downloadList.get(0);
                }

                File file = new File(SignageSystem.storagePath + SignageSystem.MediaPath + ppm.mediaName);
                if (file.exists() &&  file.length() == ppm.mediaSize) {
                    if(ppm.status.equals(ProfilePlaylistMedia.PlaylistMedia_Status.Init)) {
                        // send api to update download complete
                        ppm.status = ProfilePlaylistMedia.PlaylistMedia_Status.Complete;
                        sendProfilePlaylistMedia(ppm);
                    } else {
                        downloadList.remove(0);
                    }

                    failCountDown = 10;
                } else {
                    if (ContentService.getContent(ppm.mediaName, ppm.mediaURL)) {
                        isSuccessFullyDownload = true;

                        // send api to update download complete
                        ppm.status = ProfilePlaylistMedia.PlaylistMedia_Status.Complete;
                        sendProfilePlaylistMedia(ppm);

                        SignageSystem.local.transferLogs.add(ppm);
                        SignageSystem.saveTransferLog();

                        failCountDown = 10;
                    } else {
                        isSuccessFullyDownload = false;
                        failCountDown--;
                        if (failCountDown == 0) {
                            // send api to update download error
                            ppm.status = ProfilePlaylistMedia.PlaylistMedia_Status.Error;
                            sendProfilePlaylistMedia(ppm);

                            LogHandler.writeLog(ThisClassName, "run", LOG_TYPE.NETWORK, "Fail to download 10 times, remove content in downloadList.");
                            break;
                        }
                    }
                }

                try {
                    Thread.sleep(500);
                } catch(InterruptedException ex) { }
            }
            if(IsCancelled) {
                mIDownloadListener.DownloadCancelled((null));
            }
            else if (isSuccessFullyDownload) {
                mIDownloadListener.DownloadFinishedWithSuccessful(null);
            } else {
                mIDownloadListener.DownloadFinishedWithFailure(null);
            }
            return null;
        }
    }

    /*
    private class DownloadThread extends Thread {
        private boolean isSuccessFullyDownload = true;
        @Override
        public void run() {
            ProfilePlaylistMedia ppm = null;
            while (downloadList.size() > 0) {
                if(downloadProcessCancel) {
                    LogHandler.writeLog(ThisClassName, "run", LOG_TYPE.NETWORK, "download process cancelled.");
                    break;
                }

                if (failCountDown == 10) {
                    ppm = downloadList.get(0);
                }

                File file = new File(SignageSystem.storagePath + SignageSystem.MediaPath + ppm.mediaName);
                if (file.exists() &&  file.length() == ppm.mediaSize) {
                    if(ppm.status.equals(ProfilePlaylistMedia.PlaylistMedia_Status.Init)) {
                        // send api to update download complete
                        ppm.status = ProfilePlaylistMedia.PlaylistMedia_Status.Complete;
                        sendProfilePlaylistMedia(ppm);
                    } else {
                        downloadList.remove(0);
                    }

                    failCountDown = 10;
                } else {
                    if (ContentService.getContent(ppm.mediaName, ppm.mediaURL)) {
                        isSuccessFullyDownload = true;

                        // send api to update download complete
                        ppm.status = ProfilePlaylistMedia.PlaylistMedia_Status.Complete;
                        sendProfilePlaylistMedia(ppm);

                        SignageSystem.local.transferLogs.add(ppm);
                        SignageSystem.saveTransferLog();

                        failCountDown = 10;
                    } else {
                        isSuccessFullyDownload = false;
                        failCountDown--;
                        if (failCountDown == 0) {
                            // send api to update download error
                            ppm.status = ProfilePlaylistMedia.PlaylistMedia_Status.Error;
                            sendProfilePlaylistMedia(ppm);

                            LogHandler.writeLog(ThisClassName, "run", LOG_TYPE.NETWORK, "Fail to download 10 times, remove content in downloadList.");
                            break;
                        }
                    }
                }
                try {
                    Thread.sleep(500);
                } catch(InterruptedException ex) { }
            }

            if(downloadProcessCancel) {
                mIDownloadListener.DownloadCancelled((null));
            }
            else if (isSuccessFullyDownload) {
                mIDownloadListener.DownloadFinishedWithSuccessful(null);
            } else {
                mIDownloadListener.DownloadFinishedWithFailure(null);
            }
        }
    }*/
}
