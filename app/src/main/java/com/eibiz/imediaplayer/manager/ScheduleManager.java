package com.eibiz.imediaplayer.manager;

import android.os.AsyncTask;
import android.os.Build;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ProfileSchedule;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;

public class ScheduleManager {
    private static final String ThisClassName = ScheduleManager.class.getSimpleName();
    // Schedule EVENT.
    public class ScheduleEvent extends EventObject {
        public ScheduleEvent(Object source) {
            super(source);
        }
    }
    public interface IScheduleListener {
        void ScheduleEnter(Object sender, ScheduleManager.ScheduleEvent e);
        void ScheduleExit(Object sender, ScheduleManager.ScheduleEvent e);
    }
    public ScheduleManager.IScheduleListener mIScheduleListener = null;
    public void setIScheduleListener(ScheduleManager.IScheduleListener pIScheduleListener) {
        mIScheduleListener = pIScheduleListener;
    }

    private ScheduleManager.ScheduleAsyncTask scheduleAsyncTask = null;
    private ProfileSchedule current_Schedule = null;

    public ScheduleManager() { updateSchedule(); }

    public void updateSchedule() {
        try {
            if(scheduleAsyncTask != null)
                if(!scheduleAsyncTask.IsCancelled)
                    scheduleAsyncTask.onCancelled();

            Thread.sleep(1000);
            scheduleAsyncTask = new ScheduleManager.ScheduleAsyncTask();
            scheduleAsyncTask.IsCancelled = false;

            if (Build.VERSION.SDK_INT >= 11)
                scheduleAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                scheduleAsyncTask.execute();
        } catch(Exception ex) {
            LogHandler.writeLog(ThisClassName, "updateSchedule", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
        }
    }

    private Date convertDateToSameTodayDate(Date date, Calendar today) {
        Calendar temp = Calendar.getInstance();
        temp.setTime(date);
        temp.set(Calendar.YEAR, today.get(Calendar.YEAR));
        temp.set(Calendar.MONTH, today.get(Calendar.MONTH));
        temp.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH));
        return temp.getTime();
    }

    private ProfileSchedule getCurrentSchedule() {
        ProfileSchedule newS = null;
        try {
            if (SignageSystem.local.schedules.size() > 0) {
                for (int i = 0; i < SignageSystem.local.schedules.size(); i++) {
                    ProfileSchedule eachSchedule = SignageSystem.local.schedules.get(i);

                    Date curDate = new Date(); // get today
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(curDate);
                    curDate =  cal.getTime();

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    Date startDate = formatter.parse(eachSchedule.activeStartDateTime);
                    Date endDate = formatter.parse(eachSchedule.activeEndDateTime);

//                    String curDateStr =  DateHelper.convertDateToString(curDate);
//                    String startDateStr = DateHelper.convertDateToString(startDate);
//                    String endDateStr = DateHelper.convertDateToString(endDate);
//                    System.out.println("CurDate : " + curDateStr + ", startDate : " + startDateStr + ", endDate : " + endDateStr);

                    if(eachSchedule.ScheduleType.equals(ProfileSchedule.Sch_Type.Daily)) {
                        if ((curDate.compareTo(startDate) >= 0) && (curDate.compareTo(endDate) <= 0)) { // if today is between each_startDate and each_endDate
                            // if currentTime is between startTime and endTime
                            Date startTime = convertDateToSameTodayDate(startDate, cal);
                            Date endTime = convertDateToSameTodayDate(endDate, cal);

//                            String curDateStr =  DateHelper.convertDateToString(curDate);
//                            String startTimeStr = DateHelper.convertDateToString(startTime);
//                            String endTimeStr = DateHelper.convertDateToString(endTime);
//                            System.out.println("CurDate : " + curDateStr + ", starttime : " + startTimeStr + ", endDate : " + endTimeStr);

                            if((curDate.compareTo(startTime) >= 0) && (curDate.compareTo(endTime) <= 0)) {
                                newS = eachSchedule;
                            }
                        }
                    } else if(eachSchedule.ScheduleType.equals(ProfileSchedule.Sch_Type.Weekly)) {
                        SimpleDateFormat dayOfWeek_formatter = new SimpleDateFormat("EEEE");
                        String temp = dayOfWeek_formatter.format(curDate);
                        if(eachSchedule.ScheduleWeekly.contains(temp)) {     // if today day of week is matching with scheduleWeekly
                            if ((curDate.compareTo(startDate) >= 0) && (curDate.compareTo(endDate) <= 0)) { // if today is between each_startDate and each_endDate
                                // if currentTime is between startTime and endTime
                                Date startTime = convertDateToSameTodayDate(startDate, cal);
                                Date endTime = convertDateToSameTodayDate(endDate, cal);
                                if((curDate.compareTo(startTime) >= 0) && (curDate.compareTo(endTime) <= 0)) {
                                    newS = eachSchedule;
                                }
                            }
                        }
                    } else if(eachSchedule.ScheduleType.equals(ProfileSchedule.Sch_Type.Monthly)) {
                        int current_day = cal.get(Calendar.DAY_OF_MONTH);
                        cal.add(Calendar.MONTH, 1);
                        cal.set(Calendar.DAY_OF_MONTH, 1);
                        cal.add(Calendar.DATE, -1);
                        int lastDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

                        if(eachSchedule.ScheduleMonthly.contains(current_day)) {    // if today day is matching with scheduleMonthly
                            if ((curDate.compareTo(startDate) >= 0) && (curDate.compareTo(endDate) <= 0)) { // if today is between each_startDate and each_endDate
                                // if currentTime is between startTime and endTime
                                Date startTime = convertDateToSameTodayDate(startDate, cal);
                                Date endTime = convertDateToSameTodayDate(endDate, cal);
                                if((curDate.compareTo(startTime) >= 0) && (curDate.compareTo(endTime) <= 0)) {
                                    newS = eachSchedule;
                                }
                            }
                        } else {                                                    // if today day is not matching with scheduleMonthly
                            if(eachSchedule.ScheduleMonthly.contains(-1)) {         // if scheduleMonthly included last day of Month
                                if(current_day == lastDayOfMonth) {                 // if today day is matching with last day of Month
                                    if ((curDate.compareTo(startDate) >= 0) && (curDate.compareTo(endDate) <= 0)) { // if today is between each_startDate and each_endDate
                                        // if currentTime is between startTime and endTime
                                        Date startTime = convertDateToSameTodayDate(startDate, cal);
                                        Date endTime = convertDateToSameTodayDate(endDate, cal);
                                        if((curDate.compareTo(startTime) >= 0) && (curDate.compareTo(endTime) <= 0)) {
                                            newS = eachSchedule;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LogHandler.writeLog(ThisClassName, "getCurrentSchedule", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            newS = null;
        }
        return newS;
    }

    private class ScheduleAsyncTask extends AsyncTask<Void, Void, Void> {
        boolean IsCancelled = false;

        @Override
        protected void onCancelled() {
            super.onCancelled();
            IsCancelled = true;
            scheduleAsyncTask.cancel(IsCancelled);
        }

        @Override
        protected Void doInBackground(Void... params) {

            if(current_Schedule != null) {
                mIScheduleListener.ScheduleExit(current_Schedule, null);
                current_Schedule = null;
            }

            while (!IsCancelled) {
                ProfileSchedule new_Schedule = getCurrentSchedule();

                if(new_Schedule != null) {
                    if (current_Schedule == null) {
                        current_Schedule = new_Schedule;
                        LogHandler.writeLog(ThisClassName, "doInBackground", LOG_TYPE.INFO, "ScheduleEnter[StartDateTime, EndDateTime, idProfileSchedule]="
                                + current_Schedule.activeStartDateTime + ", " + current_Schedule.activeEndDateTime + ", " +current_Schedule.idProfileSchedule);
                        mIScheduleListener.ScheduleEnter(current_Schedule, null);
                    } else {
                        if (current_Schedule.idProfileSchedule != new_Schedule.idProfileSchedule) {
                            LogHandler.writeLog(ThisClassName, "doInBackground", LOG_TYPE.INFO, "ScheduleExit[StartDateTime, EndDateTime, idProfileSchedule]="
                                    + current_Schedule.activeStartDateTime + ", " + current_Schedule.activeEndDateTime + ", " +current_Schedule.idProfileSchedule);
                            mIScheduleListener.ScheduleExit(current_Schedule, null);
                            current_Schedule = new_Schedule;
                            LogHandler.writeLog(ThisClassName, "doInBackground", LOG_TYPE.INFO, "ScheduleEnter[StartDateTime, EndDateTime, idProfileSchedule]="
                                    + current_Schedule.activeStartDateTime + ", " + current_Schedule.activeEndDateTime + ", " +current_Schedule.idProfileSchedule);
                            mIScheduleListener.ScheduleEnter(current_Schedule, null);
                        }
                    }
                } else {
                    if(current_Schedule != null) {
                        LogHandler.writeLog(ThisClassName, "doInBackground", LOG_TYPE.INFO, "ScheduleExit[StartDateTime, EndDateTime, idProfileSchedule]="
                                + current_Schedule.activeStartDateTime + ", " + current_Schedule.activeEndDateTime + ", " +current_Schedule.idProfileSchedule);
                        mIScheduleListener.ScheduleExit(current_Schedule, null);
                        current_Schedule = null;
                    }
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    LogHandler.writeLog(ThisClassName, "doInBackground", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                }
            }
            return null;
        }
    }
}
