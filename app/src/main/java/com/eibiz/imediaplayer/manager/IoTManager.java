package com.eibiz.imediaplayer.manager;

import android.os.AsyncTask;
import android.os.Build;
import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.service.PlayerService;
import com.eibiz.imediaplayer.utility.IoTService;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;
import java.util.EventObject;

public class IoTManager {
    private static final String ThisClassName = IoTManager.class.getSimpleName();
    private IoTAsyncTask iotAsyncTask = null;
    private static boolean calledCreatePlayerIoTAPISuccess = false;
//    private static AWSIotMqttManager mqttManager;
    private static final int maxDelay = 30000;
    private static boolean displayQR = false;

    // IoTManager EVENT.
    public class IoTEvent extends EventObject {
        public IoTEvent(Object source) {
            super(source);
        }
    }
    public interface IIoTListener {
        void DownloadComplete(IoTEvent e);
        void SettingComplete(IoTEvent e, String path);
        void DisplayQRCode(IoTEvent e);
        void CompleteRegistration(IoTEvent e);
    }
    public static IIoTListener mIIoTListener = null;
    public void setIIoTListener(IIoTListener pIIoTListener) {
        mIIoTListener = pIIoTListener;
    }


    // IoTService Listener
    IoTService.IIoTServiceListener mIIoTServiceListener = new IoTService.IIoTServiceListener() {
        @Override
        public void IoTConnectionFail(IoTService.IoTServiceEvent e) {
            try {
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIIoTServiceListener.IoTConnectionFail", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }

        @Override
        public void SubscribeMessageReceive(IoTService.IoTServiceEvent e) {
            try {
            } catch(Exception ex) {
                LogHandler.writeLog(ThisClassName, "mIIoTServiceListener.SubscribeMessageReceive", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
            }
        }
    };

    public IoTManager() {

        iotAsyncTask = new IoTAsyncTask();
        if (Build.VERSION.SDK_INT >= 11)
            iotAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            iotAsyncTask.execute();
    }

    private int getRandomInt(int max, int min) {
        return ((int)(Math.random()*(max - min)) + min);
    }

    private class IoTAsyncTask extends AsyncTask<Void, Void, Void> {
        protected Void doInBackground(Void... params) {
            if(SignageSystem.local.deviceSetting.smartDelay == -1) {
                SignageSystem.local.deviceSetting.smartDelay = getRandomInt(300, 0) * 100;
                SignageSystem.saveDeviceSetting();
            }
            try {
                System.out.println("1************************************Sleep : " + SignageSystem.local.deviceSetting.smartDelay);
                Thread.sleep(SignageSystem.local.deviceSetting.smartDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while(true) {
                // call createIoTPlayer
                try {
                    if(!calledCreatePlayerIoTAPISuccess) {
                        SignageSystem.server.player = PlayerService.createIoTPlayer();                // sending API createPlayer to server.

                        if (SignageSystem.server.player != null
                                && SignageSystem.server.player.idPlayer != 0 && SignageSystem.server.player.idCompany != 0) {                 // device success to get data from server
                            SignageSystem.local.player.idCompany = SignageSystem.server.player.idCompany;
                            SignageSystem.local.player.idPlayer = SignageSystem.server.player.idPlayer;
                            SignageSystem.local.player.name = SignageSystem.server.player.name;
                            SignageSystem.local.deviceSetting.smartDelay = maxDelay;            // 30 sec

                            if (displayQR) {
                                mIIoTListener.CompleteRegistration(null);
                                displayQR = false;
                            }

                            IoTService iotSevice = new IoTService();
                            iotSevice.executeIoTStatusTask();

                            calledCreatePlayerIoTAPISuccess = true;
                        } else {                                                                // device fail to get data from server
                            if (SignageSystem.server.player.idPlayer != 0 && SignageSystem.server.player.idCompany == 0) {
                                SignageSystem.local.deviceSetting.smartDelay = maxDelay;        // success to assign in server but not assigned company yet

                                mIIoTListener.DisplayQRCode(null);
                                displayQR = true;
                            } else {                                                            // fail to assign in server
                                SignageSystem.local.deviceSetting.smartDelay = getRandomInt(300, 0) * 100;
                                SignageSystem.saveDeviceSettingJson();                                // generate SmartDelay Time
                            }
                        }
                    } else {
                        if(SignageSystem.local.player.idPlayer != 0 && SignageSystem.local.player.idCompany != 0) {
                            PlayerService.updateViewerInfoIoT();
                        }
                    }
                } catch (Exception ex) {
                    LogHandler.writeLog(ThisClassName, "IoTAsyncTask", LOG_TYPE.INTERNAL_ERROR, ex.getMessage());
                } finally {
                    try {
                        //Thread.sleep(1000);
                        Thread.sleep(SignageSystem.local.deviceSetting.smartDelay);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }
}
