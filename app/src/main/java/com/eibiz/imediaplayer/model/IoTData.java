package com.eibiz.imediaplayer.model;

public class IoTData {
    public String thingName = "";
    public String thingArn = "";
    public String thingId = "";
    public String certificateArn = "";
    public String certificateId = "";
    public String certificatePem = "";
    public String publicKey = "";
    public String privateKey = "";

    public String keystoreName = "eibiz.keystore";
    public String keystorePassword = "eibiz1234";
}
