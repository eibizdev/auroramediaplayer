package com.eibiz.imediaplayer.model;

public class PublishProfile {
    public int idPlayerPublishHistory = 0;
    public String status = Work_Status.None; // "None" for player only. "Init", "Execute", "Downloading", "Error", "Complete", "Cancel"
    public String reason = Work_Reason.None;

    public class Work_Status {
        public static final String None = "None";
        public static final String Init = "Init";
        public static final String Execute = "Execute";
        public static final String Error = "Error";
        public static final String Complete = "Complete";
        public static final String Cancel = "Cancel";
    }
    public class Work_Reason {
        public static final String None = "None";
        public static final String Downloading = "Downloading";
        public static final String FailToDownloadContent = "FailToDownloadContent";
        public static final String ServerRequested = "ServerRequested";
    }
}
