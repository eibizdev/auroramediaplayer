package com.eibiz.imediaplayer.model;

import java.util.ArrayList;
import java.util.List;

public class ServerData {
    public Player player = new Player();
    public PublishProfile publishProfile = new PublishProfile();    // new request
    public List<ProfileSchedule> schedules = new ArrayList<>();
    public List<ProfilePlaylistMedia> playlistMedias = new ArrayList<>();

    public void clearData() {
        schedules.clear();
        playlistMedias.clear();
    }
}
