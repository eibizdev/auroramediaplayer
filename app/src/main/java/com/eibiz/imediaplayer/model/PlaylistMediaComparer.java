package com.eibiz.imediaplayer.model;

import java.util.Comparator;

public class PlaylistMediaComparer implements Comparator<ProfilePlaylistMedia> {
    @Override
    public int compare(ProfilePlaylistMedia x, ProfilePlaylistMedia y) {
        return compare(x.playlistMediaOrder, y.playlistMediaOrder);
    }

    private int compare(int a, int b) {
        return a < b ? -1
                : a > b ? 1
                : 0;
    }
}
