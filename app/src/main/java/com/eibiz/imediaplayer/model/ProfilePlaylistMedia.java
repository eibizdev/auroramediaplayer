package com.eibiz.imediaplayer.model;

public class ProfilePlaylistMedia {
    public int idProfilePlaylistMedia = 0;
    public int idProfileSchedule = 0;
    //public int idPlaylistMedia = 0;
    public int playlistMediaOrder = 0;
    public int playlistMediaLength = 0;
    //public int idMedia = 0;
    public String mediaName = "";
    public String mediaType = PlaylistMedia_MediaType.None;
    public String mediaDisplayName = "";
    public String mediaURL = "";
    public String status = PlaylistMedia_Status.None;
    public String reason = PlaylistMedia_Reason.None;
    public int mediaSize = 0;

    public class PlaylistMedia_MediaType {
        public static final String None = "None";
        public static final String Image = "image";
        public static final String Video = "video";
    }
    public class PlaylistMedia_Status {
        public static final String None = "None";
        public static final String Init = "Init";
        public static final String Execute = "Execute";
        public static final String Complete = "Complete";
        public static final String Error = "Error";
    }
    public class PlaylistMedia_Reason {
        public static final String None = "None";
        public static final String FailToDownloadContent = "FailToDownloadContent";
    }
}
