package com.eibiz.imediaplayer.model;

import java.util.ArrayList;
import java.util.List;

public class LocalData {
    public DeviceSetting deviceSetting = new DeviceSetting();
    public Player player = new Player();
    public List<ProfileSchedule> schedules = new ArrayList<>();
    public List<ProfilePlaylistMedia> playlistMedias = new ArrayList<>();
    public List<ProfilePlaylistMedia> transferLogs = new ArrayList<>();

    public PublishProfile curPublishProfile = new PublishProfile();         // curRequest
    public ProfileSchedule curSchedule = new ProfileSchedule();

    public void clearData() {
        curPublishProfile = new PublishProfile();
    }
}
