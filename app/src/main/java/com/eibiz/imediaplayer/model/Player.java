package com.eibiz.imediaplayer.model;

public class Player {
    public int idPlayer = 0;
    public int idCompany = 0;
    public String name = "";
    public String macAddress = "";
    public String ipAddress = "";
    public String status = Player_Status.Offline;
    public String reason = Player_Reason.DeviceStart;
    public String screenRatio = "";
    public String screenDimension = "";
    public String deviceModel = "";
    public String appVersion = "";
    public String createBy = "Device";
    public double latitude = 0;
    public double longitude = 0;

    public class Player_Status {
        public static final String Online = "Online";
        public static final String Offline = "Offline";
        public static final String Error = "Error";
    }
    public class Player_Reason {
        public static final String None = "None";
        public static final String DeviceStart = "DeviceStart";
    }
}
