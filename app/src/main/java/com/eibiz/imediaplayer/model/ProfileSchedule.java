package com.eibiz.imediaplayer.model;

import java.util.ArrayList;
import java.util.List;

public class ProfileSchedule {
    public int idProfileSchedule = 0;
    public int idPlayerPublishHistory = 0;
    public int idPlaylist = 0;
    public String playlistName = "";
    public int idSchedule = 0;
    public String activeStartDate = "";         // use for convert from UTC format to local
    public String activeEndDate = "";           // use for convert from UTC format to local
    public String activeStartTime = "";         // use for convert from UTC format to local
    public String activeEndTime = "";           // use for convert from UTC format to local
    public String activeStartDateTime = "";
    public String activeEndDateTime = "";
    public int freqType = 0;
    public String ScheduleType = Sch_Type.Daily;
    public int freqInterval = 0;
    public List<String> ScheduleWeekly = new ArrayList<String>();
    public List<Integer> ScheduleMonthly = new ArrayList<Integer>(); // if included -1. means lastDayOFMonth

    public class Sch_Type {
        public static final String Daily = "Daily";
        public static final String Weekly = "Weekly";
        public static final String Monthly = "Monthly";
    }
    public class Week_Type {
        public static final String Monday = "Monday";
        public static final String Tuesday = "Tuesday";
        public static final String Wednesday = "Wednesday";
        public static final String Thursday = "Thursday";
        public static final String Friday = "Friday";
        public static final String Saturday = "Saturday";
        public static final String Sunday = "Sunday";
    }
}
