package com.eibiz.imediaplayer.model;

public class ConfigInfo {
    // For Local:
//    public static final String serverIPAddress = "http://192.168.1.169:3005/";
//    public static final String QRCodeServer = "http://192.168.1.165:8080/registration/";
//    public static final String IoTServer = "a3iqn8jwlskn2q-ats.iot.ap-southeast-1.amazonaws.com";
//    public static final String connectionType = ConnectionType.IoT;
//    public static final String PUBLISH_TOPIC = "eibiz/aurora/" + ENV.dev + "/message/";
//    public static final String SUBSCRIBE_TOPIC = "eibiz/aurora/" + ENV.dev + "/receive/";

    // For Dev:
//    public static final String serverIPAddress = "https://ubd0n1da90.execute-api.ap-southeast-1.amazonaws.com/dev/";
//    public static final String QRCodeServer = "http://aurora-framework-web-dev.s3-website-ap-southeast-1.amazonaws.com/registration/";
//    public static final String IoTServer = "a2qf00n24aecvj-ats.iot.ap-southeast-1.amazonaws.com";
//    public static final String connectionType = ConnectionType.IoT;
//    public static final String PUBLISH_TOPIC = "eibiz/aurora/" + ENV.dev + "/message/";
//    public static final String SUBSCRIBE_TOPIC = "eibiz/aurora/" + ENV.dev + "/receive/";

    // For Test:
    public static final String serverIPAddress = "https://u50fei6bmd.execute-api.ap-southeast-1.amazonaws.com/test/";
    public static final String QRCodeServer = "http://aurora-framework-web-test.s3-website-ap-southeast-1.amazonaws.com/registration/";
    public static final String IoTServer = "a2qf00n24aecvj-ats.iot.ap-southeast-1.amazonaws.com";
    public static final String connectionType = ConnectionType.RestAPI;
    public static final String PUBLISH_TOPIC = "eibiz/aurora/" + ENV.test + "/message/";
    public static final String SUBSCRIBE_TOPIC = "eibiz/aurora/" + ENV.test + "/receive/";

    public class ConnectionType {
        public static final String RestAPI = "RestAPI";
        public static final String IoT = "IoT";
    }

    private class ENV {
        public static final String dev = "dev";
        public static final String test = "test";
        public static final String prod = "prod";
    }
}
