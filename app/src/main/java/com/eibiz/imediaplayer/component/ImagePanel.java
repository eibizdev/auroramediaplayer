package com.eibiz.imediaplayer.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.CountDownTimer;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageView;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.io.File;
import java.io.FileInputStream;
import java.util.EventObject;

public class ImagePanel extends AppCompatImageView {

    public class ImagePanelCountDownTimer extends CountDownTimer {
        public ImagePanelCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            //System.out.println("ImagePanel finished Raise Event to PlaylistManger");
            mImagePanelListener.EndMedia(null);
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

    }

    private Context context;

    private static final String ThisClassName = ImagePanel.class.getSimpleName();

    private ProfilePlaylistMedia curPPM;
    String path = "";
    ImagePanelCountDownTimer counter;

    // ImagePanel EVENT
    public class ImagePanelEvent extends EventObject {
        public ImagePanelEvent(Object source) {
            super(source);
        }
    }

    public interface ImagePanelListener {
        void EndMedia(ImagePanelEvent e);
    }

    public ImagePanelListener mImagePanelListener;

    public void setImagePanelListener(ImagePanelListener pImagePanelListener) {
        mImagePanelListener = pImagePanelListener;
    }

    public ImagePanel(Context pContext, ProfilePlaylistMedia ppm) throws Exception {
        super(pContext);
        try {
            context = pContext;
            curPPM = ppm;
        } catch (Exception ex) {
            throw new Exception("<" + ThisClassName + "-ImagePanel[step01]> : " + ex.getMessage());
        }
    }

    public void playerStart() throws Exception {
        try {
            path = SignageSystem.storagePath + SignageSystem.MediaPath + curPPM.mediaName;

            LogHandler.writeLog(ThisClassName, "playerStart", LOG_TYPE.INFO, "Path=" + path);

            //setImageURI(Uri.parse(path));
            Bitmap myBitmap = decodeFile(new File(path));

            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            }
            else if (orientation == 3) {
                matrix.postRotate(180);
            }
            else if (orientation == 8) {
                matrix.postRotate(270);
            }
            myBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);

            setImageBitmap(myBitmap);

            setScaleType(ScaleType.FIT_CENTER);
            setVisibility(View.VISIBLE);

            if(curPPM.playlistMediaLength != -1) {
                System.out.println("ImagePanel counter start");
                counter = new ImagePanelCountDownTimer(curPPM.playlistMediaLength * 1000, 1000);
                counter.start();
            }

        } catch(Exception ex) {
            mImagePanelListener.EndMedia(null);
            throw new Exception("<" + ThisClassName + "-playerStart[step01]> : " + ex.getMessage());
        }
    }

    private Bitmap decodeFile(File f) throws Exception {
        Bitmap b = null;

        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            // The new size we want to scale to
            final int IMAGE_MAX_SIZE=1000;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;


            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int)Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                        (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            b = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);

        } catch (Exception ex) {
            throw new Exception("<" + ThisClassName + "-decodeFile[step01]> : " + ex.getMessage());
        }

        return b;
    }

    public void playerClose() throws Exception {
        try {
            if(counter != null) {
                counter.cancel();
                counter = null;
            }
        } catch(Exception ex) {
            throw new Exception("<" + ThisClassName + "-playerClose[step01]> : " + ex.getMessage());
        }
    }
}
