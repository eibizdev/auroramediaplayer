package com.eibiz.imediaplayer.component;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.Surface;
import android.view.TextureView;
import android.widget.RelativeLayout;

import com.eibiz.imediaplayer.SignageSystem;
import com.eibiz.imediaplayer.model.ProfilePlaylistMedia;
import com.eibiz.imediaplayer.utility.LOG_TYPE;
import com.eibiz.imediaplayer.utility.LogHandler;

import java.util.EventObject;

public class VideoPanel extends RelativeLayout {
    private Context context = null;
    private static final String ThisClassName = VideoPanel.class.getSimpleName();
    private MediaPlayer player;
    private ProfilePlaylistMedia curPPM;
    String path = "";
    TextureView textureView;
    SurfaceTexture surfaceTexture;

    // VideoPanel EVENT
    public class VideoPanelEvent extends EventObject {
        public VideoPanelEvent(Object source) {
            super(source);
        }
    }
    public interface VideoPanelListener {
        void OnLoaded(VideoPanelEvent e);
        void EndMedia(VideoPanelEvent e);
        void ErrorMedia(VideoPanelEvent e);
    }
    public VideoPanelListener listenerList;
    public void setVideoPanelListener(VideoPanelListener l) {
        listenerList = l;
    }

    TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            surfaceTexture=surface;
        }
        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {return false;}
        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {}
        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {}
    };
    MediaPlayer.OnErrorListener ErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer m, int what, int extra) {
            try {
                if(player != null) {
                    player.stop();
                    player.reset();
                    player = null;
                }
                listenerList.ErrorMedia(null);
            } catch(Exception e) {
                LogHandler.writeLog(ThisClassName, "onError", LOG_TYPE.INTERNAL_ERROR, e.getMessage());
            }
            return true;
        }
    };
    MediaPlayer.OnCompletionListener CompletedListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer m) {
            try {
                if(player != null) {
                    player.stop();
                    player.reset();
                    player = null;
                }
                listenerList.EndMedia(null);
            } catch(Exception e) {
                LogHandler.writeLog(ThisClassName, "onCompletion", LOG_TYPE.INTERNAL_ERROR, e.getMessage());
            }

        }
    };
    MediaPlayer.OnPreparedListener PreparedListener = new MediaPlayer.OnPreparedListener(){
        @Override
        public void onPrepared(MediaPlayer m) {
            try {
                player = m;
                textureView.setLayoutParams(new RelativeLayout.LayoutParams(SignageSystem.fullScreenWidth, SignageSystem.fullScreenHeight));

                m.setLooping(false);

                listenerList.OnLoaded(null);
            } catch (Exception e) {
                LogHandler.writeLog(ThisClassName, "onPrepared", LOG_TYPE.INTERNAL_ERROR, e.getMessage());
                e.printStackTrace();
            }
        }
    };

    public VideoPanel(Context pContext, ProfilePlaylistMedia ppm) throws Exception {
        super(pContext);

        try {
            textureView = new TextureView(pContext);
            textureView.setSurfaceTextureListener(surfaceTextureListener);
            this.addView(textureView);

            context = pContext;
            curPPM = ppm;

            path = SignageSystem.storagePath + SignageSystem.MediaPath + curPPM.mediaName;
            LogHandler.writeLog(ThisClassName, "VideoPanel", LOG_TYPE.INFO, "Path=" + path);
            player = new MediaPlayer();
            // Play video when the media source is ready for playback.
            player.setDataSource(path);
            player.setOnPreparedListener(PreparedListener);
            player.setOnCompletionListener(CompletedListener);

            player.prepare();
        } catch(Exception ex) {
            throw new Exception("<VideoPanel-VideoPanel[step01]> : " + ex.getMessage());
        }
    }
    public void playerStart() throws Exception {
        try {
            if(player != null) {
                //LogHandler.writeLog(ThisClassName, "playerStart", LOG_TYPE.INFO, "Path=" + path);
                player.setSurface(new Surface(surfaceTexture));
                player.seekTo(0);
                player.start();
            }
        } catch(Exception ex) {
            throw new Exception("<VideoPanel-playerStart[step01]> : " + ex.getMessage());
        }
    }
    public void playerClose() throws Exception {
        try {
            if(player != null) {
                player.stop();
                player.reset();
                player.release();
                player = null;
            }
            if(textureView!=null){
                textureView=null;
            }
            if(surfaceTexture!=null){
                surfaceTexture=null;
            }
        } catch(Exception ex) {
            throw new Exception("<VideoPanel-playerClose[step01]> : " + ex.getMessage());
        }
    }
}
